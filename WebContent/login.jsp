<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href="<c:url value="/resources/css/eshopper/bootstrap.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/css/eshopper/font-awesome.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/eshopper/main.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/eshopper/responsive.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/eshopper/jquery-ui.min.css"/>" rel="stylesheet">


<script src="<c:url value="/resources/js/eshopper/jquery-1.11.3.min.js"/>"></script>
<script src="<c:url value="/resources/js/eshopper/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/eshopper/jquery-ui.min.js"/>"></script>


<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


</head>


<body>

	<header id="header"> <!--header-->
	<div class="header-middle">
		<!--header-middle-->
		<div class="container">
			<div class="row" style="vertical-align: middle;">
				<div class="col-sm-4">
					<div class="logo pull-left">
						<a href=""><img
							src="<c:url value="/resources/images/MobileCard_header_600.PNG"/>"
							alt="" /></a>
					</div>
				</div>

			</div>
		</div>
	</div>
	</header>

	<section>
	<div class="container">
		<div class="tab-content">
			<div class="tab-pane fade  active in " id="login">
				<div class="shopper-informations">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="login-form">

							<font size='5' color='blue'>Proporcione sus credenciales</font>
							<hr>

							<form action='j_security_check' method='post'>
								<table>
									<tr>
										<td>Usuario:</td>
										<td><input type='text' name='j_username'></td>
									</tr>
									<tr>
										<td>Password:</td>
										<td><input type='password' name='j_password' size='8'></td>
									</tr>
								</table>
								<br> <input type='submit' class="btn btn-default"
									value='login'>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
