<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>


<script>
$(function() {

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');

		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});

	$("#saveForm").click(function(){
		$("#registroForm").submit(); //SUBMIT FORM
	});  


	$("#cambioPasswordSubmit").click(function(){

		$('#cambioPassword-form').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');

			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
			}else{
				$( "#cargando" ).dialog( "open" );
			}

			//event.preventDefault(); //STOP default action
			 
		});

		$("#cambioPassword-form").submit(); //SUBMIT FORM
	});  

	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
	});

	
	$("#cambioPassword-form").validate({
        rules :{
        	password : {
                required : true, 
                maxlength : 16,
            },
            newPassword : {
                required : true, 
                maxlength : 16,
            },
            newPasswordCon : {
                required : true, 
                maxlength : 16,
                equalTo: "#newPassword"
            }

        },
        messages : {
        	newPasswordCon : {
        		equalTo : "Por favor, escribir el mismo valor que el campo Nuevo Password.",
            },
        }
    });

});
</script>


</head>

	<section>
		<div class="container">
			<div class="row">
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<li class="active" ><a href="#cambioPass" data-toggle="tab">Cambio de Contraseña</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade  active in " id="cambioPass" >
							<div class="register-req">
								<h4>Bienvenido (a):  ${usuario.usrNombre}</h4>
								<c:if test="${usuario.idUsrStatus == 99}">
									<p> Por motivos de seguridad es necesario que cambie su contraseña la primera vez que ingresa. </p>
								</c:if>
								<c:if test="${usuario.idUsrStatus == 98}">
									<p> Hemos detectado que a solicitado "Recuperar su Contraseña", por tal motivo es necesario que cambie su contraseña. </p>
								</c:if>
							</div>
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form total_area ">
					    				
					    				<div class="status alert alert-danger" <c:if test="${mensajeCambPass == null}"> style="display: none" </c:if> >${mensajeCambPass}</div>
					    				
								    	<form:form id="cambioPassword-form" name="cambioPassword-form" method="post" 
								    		autocomplete="off" modelAttribute="cambioPassword"
								    		action="${pageContext.request.contextPath}/portal/pago-cambio-pass-end">
								    		<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
								    		<form:input path="login" placeholder="Usuario" />
											<form:input type="password" path="password" placeholder="Contraseña" />
											<form:input type="password" path="newPassword" placeholder="Nueva Contraseña" />
											<form:input type="password" path="newPasswordCon" placeholder="Confirmar Nueva Contraseña" />
							                
							                <button type="submit" name="cambioPasswordSubmit" class="btn btn-default">Realizar Pago</button>
								        </form:form>
					    			</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!--/category-tab-->
		</div>
	</section>
