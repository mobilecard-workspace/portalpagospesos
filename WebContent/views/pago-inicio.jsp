<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/resources/css/eshopper/jquery-ui.min.css"/>" rel="stylesheet">

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/eshopper/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/enmascara.js"/>"></script>
<%-- <script src="<c:url value="/resources/js/pago-inicio.js"/>"></script> --%>

<style type="text/css">
	.ui-dialog-titlebar { 
        display: none;
    }

</style>

<script type="text/javascript">
$(document).ready(function(){
	var dataSend = "";
	
	$('#modelo').val(jQuery.browser.name);
	$('#software').val(jQuery.browser.version);

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		closeOnEscape: false,
		autoOpen: false
	});

	$("#cargando" ).dialog( "close" );
//	$("#cargando" ).dialog( "open" );

	$('#main-form').submit(function(){
		//$("#pagoBTN").html("Realizar Pago");
		setTimeout(EnmascaraV2('tmpTarjeta','tarjeta',false),10);
		$("#pagoBTN").attr("disabled", true);
		//$("#cargando" ).dialog( "open" );
	});
	
	<c:if test="${tproveedor.isAmex == 1 }">
		isAMEX();
	
		function isAMEX(){
			if($("#tipoTarjeta").val() == 3){
				$("#dataAmex").show();
				$("#dataVISA").hide();
				
			}else{
				$("#dataAmex").hide();
				$("#dataVISA").show();

				if( $("#tarjeta" ).val() != null && $( "#tarjeta" ).val() != '' && 
						$( "#tarjeta" ).val().length > 6 && dataSend != $( "#tarjeta" ).val().substring(0, 6)){
					callMeses();
				}
			}
		}
		
		$("#tipoTarjeta").change(function(){
	        isAMEX();
		});

		
		
	</c:if>

	//setTimeout(EnmascaraV2('tmpTarjeta','tarjeta',false),10);
	
});
</script>

	<section>
		<div class="container">
			<div class="row">		
				<div class="category-tab shop-details-tab"><!--category-tab-->	
					<div align="right">																										
						<ul class="nav nav-tabs">
							<li class="active"  ><a href="${pageContext.request.contextPath}/logout">Salir</a></li>
						</ul>						
					</div>			
					<div class="col-sm-12">						
																				
						<ul class="nav nav-tabs">
							<li class="active"  ><a href="#pagodirecto" data-toggle="tab">Pago Directo</a></li>
						</ul>
						
					</div>
					<div class="tab-content">
						<div class="tab-pane fade  active in " id="pagodirecto" >
							<div class="register-req">
								<p>Favor de ingresar los siguientes datos para realizar el cargo bancario:</p>
							</div>
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form">
										<p>Informaci&oacute;n de la Tarjeta de Cr&eacute;dito / Debito. </p>
					    				
					    				<div class="status alert alert-danger" <c:if test="${mensajePagoDir == null}"> style="display: none" </c:if> >${mensajePagoDir}</div>
					    				
								    	<form:form id="main-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="pagoForm"
								    		action="${pageContext.request.contextPath}/portal/procesa-pago-directo">
								    		<input type="hidden" id="modelo" name="modelo" />
											<input type="hidden" id="software" name="software" />
							             
												
											<!-- <p>Nombre </p> -->
							                <form:input path="nombre"  required="required" placeholder="Nombre" maxlength="50" />
							                
							                <!-- <p>Email </p> -->
							                <form:input type="email" path="email"  required="required" placeholder="Correo Electronico"  maxlength="50" />
											
											<!-- <p>Ddescripci&eacute;n del cargo </p> -->
							                <form:input path="descripcion"  required="required" placeholder="Descripción del cargo bancario" maxlength="50" />
							                
							                <form:input path="importe"  required="required" placeholder="Importe del cargo bancario" maxlength="15" />
											
											<form:select path="tipoTarjeta" id="tipoTarjeta" required="required" >
											  <c:if test="${tproveedor.isVisa == 1}"> 
									              <form:option value="1" >Visa</form:option>
									              <form:option value="2" >Master Card</form:option>
								              </c:if>
								              <c:if test="${tproveedor.isAmex == 1}">
								              	<form:option value="3" >AMEX</form:option>
								              </c:if>
								          	</form:select>
											
							                <!-- <p>Numero Tarjeta </p> -->
							               <form:input path="tmpTarjeta" id="tmpTarjeta"  required="required" placeholder="Número Tarjeta" maxlength="16" 
							                	onkeydown="setTimeout(EnmascaraV2('tmpTarjeta','tarjeta',false),10);"/>
							                
							                <form:hidden path="tarjeta" id="tarjeta" required="required" maxlength="16" /> 
							            	
							            	<form:select path="mes" id="mes"  required="required">
												<form:option value="">-- Vence Mes --</form:option>
												<form:option value="01">1</form:option>
					                            <form:option value="02">2</form:option>
					                            <form:option value="03">3</form:option>
					                            <form:option value="04">4</form:option>
					                            <form:option value="05">5</form:option>
					                            <form:option value="06">6</form:option>
					                            <form:option value="07">7</form:option>
					                            <form:option value="08">8</form:option>
					                            <form:option value="09">9</form:option>
					                            <form:option value="10">10</form:option>
					                            <form:option value="11">11</form:option>
					                            <form:option value="12">12</form:option>
											</form:select >
											<form:select path="anio" id="anio"  required="required">
												<form:option selected="selected" value="">-- Vence Año --</form:option>
												<%
					                                java.util.Calendar C= java.util.Calendar.getInstance();
					                                int anio=C.get(java.util.Calendar.YEAR);
					                                //out.println("<form:option selected>"+anio+"</form:option>");
					                                //anio++;
					                                for(int i=0;i<10;i++)
					                                {
					                                	out.println("<option value='" + anio + "'>"+ anio++ +"</option>");
					                                }
					                            %>         
											</form:select >
											
											<c:if test="${tproveedor.isAmex == 1}">
												<div id="dataAmex" style="display: none;">
													<p>Dom. de EDO. Cuenta Tarjeta </p>
													<form:input path="domicilio"  placeholder="Dom. de EDO. Cuenta Tarjeta"  maxlength="100" />
													<p>C.P. de EDO. Cuenta Tarjeta </p>
													<form:input path="cp"  placeholder="C.P. de EDO. Cuenta Tarjeta"  maxlength="100" />
												</div>
											</c:if>
												
											<%-- <c:if test="${soliCompra.conMSI == true}">
												<div id="dataVISA" >
													<p> Diferir Pago a Meses:</p> 
													<form:select path="msi" id="msi" required="required">
														<form:option value="00">Ingrese Tarjeta...</form:option>
							                            <c:forTokens items="${soliCompra.arrayMeses}" delims="," var="mes">
							                            	<form:option value="${mes}">${mes}</form:option>
							                            </c:forTokens>
													</form:select >
												</div>
											</c:if> --%>
											<!-- <p>Codigo Seguridad </p> -->
							                <form:password path="cvv2" id="cvv2"  required="required" placeholder="Código Seguridad" maxlength="4" />
							                
							                <button type="submit" id="pagoBTN" class="btn btn-default">Realizar Pago</button>
								        </form:form>
					    			</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
