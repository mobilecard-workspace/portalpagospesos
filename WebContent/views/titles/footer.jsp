
<footer id="footer">
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright &copy; 
					<%
                         java.util.Calendar C= java.util.Calendar.getInstance();
                         out.println(C.get(java.util.Calendar.YEAR));
                     %>  
                      Mobilecard. Todos los derechos reservados.</p>
					<p class="pull-right">Powered by  <span><a target="_blank" href="http://www.mobilecard.com">Addcel</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
