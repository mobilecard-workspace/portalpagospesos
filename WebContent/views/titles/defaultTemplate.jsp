<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" 	prefix="tiles" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="icon" type="image/x-icon" href="<c:url value="/resources/images/default/icono_45x45.png"/>">

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
    <link href="<c:url value="/resources/css/eshopper/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/eshopper/font-awesome.min.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/css/eshopper/main.css"/>" rel="stylesheet">
	<link href="<c:url value="/resources/css/eshopper/responsive.css"/>" rel="stylesheet">
<%--     <!--[if lt IE 9]>
    <script src="<c:url value="/resources/js/eshopper/html5shiv.js"/>"></script>
    <script src="<c:url value="/resources/js/eshopper/respond.min.js"/>"></script>
    <![endif]-->
--%>       
<%--    <link rel="shortcut icon" href="<c:url value="/resources/images/eshopper/eshopper/ico/favicon.ico"/>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/images/eshopper/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/images/eshopper/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/images/eshopper/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" href="<c:url value="/resources/images/eshopper/ico/apple-touch-icon-57-precomposed.png"/>"> --%>
    
    <script src="<c:url value="/resources/js/eshopper/jquery-1.11.3.min.js"/>"></script>
	<script src="<c:url value="/resources/js/eshopper/bootstrap.min.js"/>"></script>
</head>

<body id="body" data-twttr-rendered="true">
	<div id="cargando" align="center" style="display: none; z-index: 9999">
		<p>Enviando informacion...</p>
		<%-- <p><img src="<c:url value="/resources/images/loader.gif"/>" /></p> --%>
		<%-- <p><img src="<c:url value="/resources/images/loading-spinner.gif"/>" /></p> --%>
		<p><img src="<c:url value="/resources/images/loading2.gif"/>" /></p>
	</div>
	
	<tiles:insertAttribute name="header" />
	
	<tiles:insertAttribute name="menu" ignore="true"/>
	
	<tiles:insertAttribute name="body" />

	<tiles:insertAttribute name="footer" />

</body>
</html>
