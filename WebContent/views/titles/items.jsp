<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Inicio </a></li>
				  <li class="active">Resumen del Pago</li>
				</ol>
			</div>
			<div class=" cart_info">
				<table class="table ">
					<thead>
						<tr class="cart_menu">
							<th class="description">Descripci&oacute;n</th>
							<th class="total">Importe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_description">
								<p>${soliCompra.descripcion}</p>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.importeForm}</p>
							</td>
						</tr>
						<c:if test="${soliCompra.comision > 0.0}">
							<tr>
								<td class="cart_description">
									<p>Comisi&oacute;n</p>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${soliCompra.comisionForm}</p>
								</td>
							</tr>
						</c:if>
						<tr>
							<td class="cart_description">
								<p><B>Importe Total</B></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.totalPagoForm}</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

