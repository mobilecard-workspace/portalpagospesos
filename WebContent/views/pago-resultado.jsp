<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>

<link href="<c:url value="/resources/css/default/menu.css"/>" rel="stylesheet">

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Inicio </a></li>
				  <li class="active">Resumen del Pago</li>
				</ol>
			</div>
			<c:if test="${usuario != null}">
				<div class="breadcrumbs">
					<ol class="breadcrumb">
					  <li>Estimado (a):  ${usuario.usrNombre} ${usuario.usrApellido} </a></li>
					</ol>
				</div>
			</c:if>
			<div class=" cart_info">
				<table class="table ">
					<thead>
						<tr class="cart_menu">
							<th class="description">Descripci&oacute;n</th>
							<th class="total">Importe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_description">
								<p>${pago.descripcion}</p>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">$ ${pago.importe}</p>
							</td>
						</tr>
						<%-- <c:if test="${soliCompra.comision > 0.0}">
							<tr>
								<td class="cart_description">
									<p>Comicion</p>
								</td>
								<td class="cart_total">
									<p class="cart_total_price"> $ ${pago.comisionForm}</p>
								</td>
							</tr>
						</c:if> --%>
						<tr>
							<td class="cart_description">
								<p><B>Importe Total</B></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$ ${pago.importe}</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<c:if test="${soliCompra.modoPago != 3}"> 
								<li class="active"><a href="#pagoResultado" data-toggle="tab">Resultado Pago</a></li>
							</c:if>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="pagoResultado" >
							<div class="register-req">
								<c:if test="${soliCompra.modoPago != 3}">
									<c:if test="${soliCompraResp.idError == 0}">
										<p>El proceso de pago fue Exitoso:</p>
										
									
										<p>El comprobante ha sido enviado al correo electrónico registrado.</p>
									</c:if>
									<c:if test="${soliCompraResp.idError != 0}">
										<p>El proceso de pago fue Rechazado:</p>
									</c:if>
								</c:if>
								<c:if test="${soliCompra.modoPago == 3}">
									<p>Referencia Generada con &Eacute;xito</p>
								</c:if>
							</div>
							<div class="row">
								<div class="col-sm-10 col-sm-offset-1">
									<div class="total_area login-form">
										<form id="form1" name="form1" method="post" 
								    		autocomplete="off" modelAttribute="pagoForm"
								    		action="${pageContext.request.contextPath}">
								    	<input type="hidden" id="param" name="param" value="${soliCompraResp.jsonNotificacion}" />
										<ul style="background-color: #FFFFFF">
											<c:if test="${soliCompra.modoPago != 3 && soliCompraResp.idError == 0}">
												<li>Estado: <span>Aprobado</span></li>
											</c:if>
											<c:if test="${soliCompra.modoPago != 3 && soliCompraResp.idError != 0}">
												<li>Estado: <span>Rechazado</span></li>
											</c:if>
											<c:if test="${soliCompraResp.idError != 0}">
												<li>Error: <span>${soliCompraResp.mensajeError}</span></li>
											</c:if>
											
											<li>Fecha de Transacci&oacute;n: <span>${soliCompraResp.fechaTransaccion}</span></li>
											
											<c:if test="${soliCompraResp.idError == 0 && soliCompra.modoPago != 3}">
												<li>N&uacute;mero de Aprobaci&oacute;n: <span>${soliCompraResp.autorizacionBancaria}</span></li>
												<li>N&uacute;mero de Transacci&oacute;n MC: <span>${soliCompraResp.transaccionMC}</span></li>
											</c:if>
											<c:if test="${soliCompraResp.idError == 0 && soliCompra.modoPago == 3}">
												<li>N&uacute;mero de Referencia: <span>&nbsp;</span></li>
												<li>&nbsp;<span>${soliCompraResp.referencia}</span></li>
												<li>N&uacute;mero de Transacci&oacute;n MC: <span>${soliCompraResp.transaccionMC}</span></li>
											</c:if>
										</ul>
										<button type="submit" class="btn btn-default">Terminar</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>