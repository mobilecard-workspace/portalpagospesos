<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>

	<script>
		$(function() {
			$( "input[type=submit], button" )
				.button()
				.click(function( event ) {
				//event.preventDefault();
			});
	
		});
	</script>
	
	<%-- <section id="cart_items">
		<div class="container">
			<div class="cart_info">
					<div class="has-error" style="margin: 25px 20px 20px">
						<h4>Ocurrio un error durante el proceso de Pago MobileCard:</h4>
						<!-- <label class="control-label">Descripción del Error:</label> -->
						<p> <label class="control-label">${mensajeError} </label></p>
						<a class="btn btn-default update" href="https://www.mobilecar.mx">Salir</a>
					</div>
			</div>
		</div>
	</section> --%>
<%-- 	
	<section id="cart_items">
			<div class="container">
				<div class=" cart_info">
					<table class="table ">
						<thead>
							<tr class="cart_menu">
								<th class="description"><h4>Ocurrio un error durante el proceso de Pago MobileCard:</h4></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="cart_description">
									<p>
									<div class="has-error" style="margin: 25px 20px 20px">
										<div class="status alert alert-danger" >${mensajeError}</div>
									</div>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-default update" href="https://www.mobilecar.mx">Salir</a>
				</div>
			</div>
		</section> <!--/#cart_items-->
	 --%>
	<section>
		<div class="cart_items">
			<div class="container">
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#referen" data-toggle="tab">Ocurrio un error</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="referen" >
							<div class="row" style="margin: 25px 20px 20px 20px">
									<div class="status alert alert-danger" >${mensajeError}</div>
									
									<a class="btn btn-default update" href="https://www.mobilecar.mx">Salir</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
			
