<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" 	prefix="tiles" %>

<form method="post" name="form1" action="https://via.banorte.com/payw2">
	<input type="hidden" name="ID_AFILIACION" value="${pw2.ID_AFILIACION}" /> 
	<input type="hidden" name="USUARIO" value="${pw2.USUARIO}" /> 
	<input type="hidden" name="CLAVE_USR" value="${pw2.CLAVE_USR}" /> 
	<input type="hidden" name="CMD_TRANS" value="${pw2.CMD_TRANS}" /> 
	<input type="hidden" name="ID_TERMINAL" value="${pw2.ID_TERMINAL}" /> 
	<input type="hidden" name="MONTO" value="${pw2.MONTO}" /> 
	<input type="hidden" name="MODO" value="${pw2.MODO}" /> 	 
	<input type="hidden" name="NUMERO_TARJETA" value="${pw2.NUMERO_TARJETA}" /> 
	<input type="hidden" name="FECHA_EXP" value="${pw2.FECHA_EXP}" /> 
	<input type="hidden" name="CODIGO_SEGURIDAD" value="${pw2.CODIGO_SEGURIDAD}" /> 
	<input type="hidden" name="MODO_ENTRADA" value="${pw2.MODO_ENTRADA}" />	
	<input type="hidden" name="URL_RESPUESTA" value="${pw2.URL_RESPUESTA}" />
	<input type="hidden" name="IDIOMA_RESPUESTA" value="${pw2.IDIOMA_RESPUESTA}" />	
</form>
<section id="cart_items">
	<div class="container">
		<div class=" cart_info">
			<table class="table ">
				<thead>
					<tr class="cart_menu">
						<th class="description">Env&iacute;o de informaci&oacute;n.</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="cart_description">
							<p><br>Se est&aacute; enviando la informaci&oacute;n al Banco para su procesamiento, favor de esperar.<br><br></p>
						</td>
						<tr>
							<td align="center">								
								<p><img src="<c:url value="/resources/images/loading2.gif"/>" /></p>
							</td>
						</tr>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	function sendform() {
	    setTimeout (function () {
	    	document.form1.submit();
	    }, 1000);
	}
	
	sendform();            
</script>

