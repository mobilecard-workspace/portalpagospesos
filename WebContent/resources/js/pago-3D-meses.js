
/* jQuery.validator.setDefaults({
	debug: true,
	success: "valid"
}); */
	

$(function() {
	var dataSend = "";
	
	$('#modelo').val(jQuery.browser.name);
	$('#software').val(jQuery.browser.version);

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');
		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});
	
	$('#main-form').submit(function(){
		//$("#pagoBTN").html("Realizar Pago");
		$("#pagoBTN").attr("disabled", true);
		$("#cargando" ).dialog( "open" );
	});


	$("#tarjeta").blur(function(){
		
		if( $("#tarjeta" ).val() != null && $( "#tarjeta" ).val() != '' && 
				$( "#tarjeta" ).val().length > 6 && dataSend != $( "#tarjeta" ).val().substring(0, 6)){
			$('input[type="submit"]').attr('disabled','disabled');
//			$( "#lblErrorPago" ).text("");

			var dataJson = {
	    		"tarjeta" : $( "#tarjeta" ).val(  ),
	    	};
			
			$.ajax({
				 url: '/MobilecardSJGSBridge/portal/consultaMSI/data',
				 type: 'POST',
				 data: JSON.stringify(dataJson),
				 dataType: 'json',
				 contentType: "application/json; charset=utf-8",
				 mimeType: 'application/json; charset=utf-8',
				 async: false,
	             cache: false,    //This will force requested pages not to be cached by the browser          
	             processData:false, //To avoid making query String instead of JSON
				 statusCode: {
					 404: function() {
					 alert( "Pagina no encontrada." );
					 }
				 },
				 success: function (data) {
					dataSend = $( "#tarjeta" ).val().substring(0, 6);
					$('input[type="submit"]').removeAttr('disabled');
				 	
//				 	alert(data);
			 		if(data != null){
			 			 $('#msi').empty();
			 			/*$('select option').remove();*/
			 	         //Inserta los valores recuperados en la lista desplegable
			 	         $.each(data, function(i, item) {
			 	           $('#msi').append('<option value='+item.value+'>'+item.label+'</option>');
			 	          });
			 	         //Actualiza la lista desplegable
			 	         //$('#msi').selectmenu("refresh", true);
			 	    }

				 },
				 error: function (response, textStatus, errorThrown) {
				 	alert('An error has occured!! \nresponse: ' + response +
						 	'\ntextStatus: ' +  textStatus +
						 	'\nerrorThrown: ' + errorThrown);
				 }
			}); 
		}
	});  

});