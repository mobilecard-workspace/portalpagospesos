package com.addcel.portal.pagos.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.addcel.portal.pagos.model.vo.SolicitudCompraVO;

@Component
public class UtilsValidate {
	private static final Logger logger = LoggerFactory.getLogger(UtilsValidate.class);
	
	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static final String PATTERN_DIGITS = "^[_0-9]*$";
	private static final String PATTERN_DIGITS = "^[0-9]+(\\.?[0-9]{1,2})?+$";
	
	// Compiles the given regular expression into a pattern.
	private static final  Pattern patternMail = Pattern.compile(PATTERN_EMAIL);
	private static final  Pattern patternDig = Pattern.compile(PATTERN_DIGITS);
	
	public static final String REQUIRED = "required:true";
	public static final String REQUIRED_NUM = "required_num:true";
	public static final String REMOTE = "remote:";
	public static final String EMAIL = "email:true";
	public static final String URL = "url:";
	public static final String DATE = "date:";
	public static final String DATEISO = "dateISO:";
	public static final String NUMBER = "number:";
	public static final String DIGITS = "digits:";
	public static final String EQUALTO = "equalTo:";
	public static final String ACCEPT = "accept:";
	public static final String MAXLENGTH = "maxlength:";
	public static final String MINLENGTH = "minlength:";
	public static final String RANGELENGTH = "rangelength:";
	public static final String RANGE = "range:";
	public static final String MAX = "max:";
	public static final String MIN = "min:";
	
	private static final String _REQUIRED = "required";
	private static final String _REQUIRED_NUM = "required_num";
	private static final String _REMOTE = "remote";
	private static final String _EMAIL = "email";
	private static final String _URL = "url";
	private static final String _DATE = "date";
	private static final String _DATEISO = "dateISO";
	private static final String _NUMBER = "number";
	private static final String _DIGITS = "digits";
	private static final String _EQUALTO = "equalTo";
	private static final String _ACCEPT = "accept";
	private static final String _MAXLENGTH = "maxlength";
	private static final String _MINLENGTH = "minlength";
	private static final String _RANGELENGTH = "rangelength";
	private static final String _RANGE = "range";
	private static final String _MAX = "max";
	private static final String _MIN = "min";
	
	public static final String ERROR_REQUIRED = "El campo ## es obligatorio.";
	public static final String ERROR_REMOTE = "Por favor, rellena este campo.";
	public static final String ERROR_EMAIL = "Por favor, escribir una dirección de correo válida";
	public static final String ERROR_URL = "Por favor, escribir una URL válida.";
	public static final String ERROR_DATE = "En el campo ## escribir una fecha válida (@@).";
	public static final String ERROR_DATEISO = "Por favor, escribir una fecha (ISO) válida.";
	public static final String ERROR_NUMBER = "Por favor, escribir un número entero válido.";
	public static final String ERROR_DIGITS = "El campo ## es sólo de numeros.";
	public static final String ERROR_EQUALTO = "El campo ## es diferente a el campo @@.";
	public static final String ERROR_ACCEPT = "Por favor, escribir un valor con una extensión aceptada.";
	public static final String ERROR_MAXLENGTH = "En el campo ## longitud maxima @@ caracteres.";
	public static final String ERROR_MINLENGTH = "En el campo ## longitud minima @@ caracteres.";
	public static final String ERROR_RANGELENGTH = "Por favor, escribir un valor entre {0} y {1} caracteres.";
	public static final String ERROR_RANGE = "Por favor, escribir un valor entre {0} y {1}.";
	public static final String ERROR_MAX = "Por favor, escribir un valor menor o igual a {0}.";
	public static final String ERROR_MIN = "Por favor, escribir un valor mayor o igual a {0}.";
	
	public static String validateFields(String campo, String field, String[] rules){
		StringBuffer resp = new StringBuffer();
		String [] rul = null;
		try{
			for(String rule: rules){
				rul = rule.split(":");
				
				if(rul[0].equals(_REQUIRED)){
					if(!validateRequired(campo)){
						resp.append(ERROR_REQUIRED.replaceAll("##", field)).append("</BR>");
						break;
					}
				}else
					
				if(rul[0].equals(_REQUIRED_NUM)){
					if(!validateRequiredNumerico(campo)){
						resp.append(ERROR_REQUIRED.replaceAll("##", field)).append("</BR>");
						break;
					}
				}else
				
				if(rul[0].equals(_DIGITS)){
					if(validateRequired(campo) && !validateDigits(campo)){
						resp.append(ERROR_DIGITS.replaceAll("##", field)).append("</BR>");
					}
				}else
				
				if(rul[0].equals(_MAXLENGTH)){
					if(validateRequired(campo) && !validateMaxLength(campo, Integer.parseInt(rul[1]))){
						resp.append(ERROR_MAXLENGTH.replaceAll("##", field).replaceAll("@@", rul[1])).append("</BR>");
					}
				}else
				
				if(rul[0].equals(_MINLENGTH)){
					if(validateRequired(campo) && !validateMinLength(campo, Integer.parseInt(rul[1]))){
						resp.append(ERROR_MINLENGTH.replaceAll("##", field).replaceAll("@@", rul[1])).append("</BR>");
					}
				}else
				
				if(rul[0].equals(_MAX)){
					if(validateRequired(campo) && !validateMax(campo, Integer.parseInt(rul[1]))){
						resp.append(ERROR_MAX.replaceAll("##", field).replaceAll("@@", rul[1])).append("</BR>");
					}
				}else
					
				if(rul[0].equals(_MIN)){
					if(validateRequired(campo) && !validateMin(campo, Integer.parseInt(rul[1]))){
						resp.append(ERROR_MIN.replaceAll("##", field).replaceAll("@@", rul[1])).append("</BR>");
					}
				}else
					
				if(rul[0].equals(_EMAIL)){
					if(validateRequired(campo) && !validateEmail(campo)){
						resp.append(ERROR_EMAIL.replaceAll("##", field)).append("</BR>");
					}
				}else
				
				if(rul[0].equals(_DATE)){
					if(validateRequired(campo) && !validateFormatDate(campo, rul[1])){
						resp.append(ERROR_DATE.replaceAll("##", field).replaceAll("@@", rul[1])).append("</BR>");
					}
				}else
				
				if(rul[0].equals(_EQUALTO)){
					if(validateRequired(campo) && !validateEqualTo(campo, rul[1])){
						resp.append(ERROR_EQUALTO.replaceAll("##", field).replaceAll("@@", rul[2])).append("</BR>");
					}
				}
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al validar el campo {}: {}" , field, e.getMessage());
			resp.append("Ocurrio un error al validar el campo: " ).append( field).append( ".</BR>");
		}
		
		return resp.toString();
	}

	public static boolean validateRequired(String cadena) {
		boolean resp = false;
		try {
			if (!StringUtils.isEmpty(cadena) && !" ".equals(cadena)) {
				resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo requerido: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateRequiredNumerico(String cadena) {
		boolean resp = false;
		double num = 0;
		try {
			if (!StringUtils.isEmpty(cadena) && !" ".equals(cadena)) {
				num = Double.parseDouble(cadena);
				if(num > 0.0)
					resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo requerido: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateDigits(String cadena) {
		boolean resp = false;
		try {
			// Match the given input against this pattern
	        resp = patternDig.matcher(cadena).matches();
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo digitos: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateMaxLength(String cadena, int maxLengt) {
		boolean resp = false;
		try {
			if (!StringUtils.isEmpty(cadena) && cadena.length() <= maxLengt) {
				resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo longitud maxima: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateMinLength(String cadena, int minLengt) {
		boolean resp = false;
		try {
			if (!StringUtils.isEmpty(cadena) && cadena.length() >= minLengt) {
				resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo longitud minima: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateMax(String cadena, int maxLengt) {
		boolean resp = false;
		try {
			if (Integer.parseInt(cadena) <= maxLengt) {
				resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo longitud maxima: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateMin(String cadena, int minLengt) {
		boolean resp = false;
		try {
			if (Integer.parseInt(cadena) >= minLengt) {
				resp = true;
			}
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo longitud maxima: {} es: {}", cadena, resp);
		return resp;
	}
	
	public static boolean validateFormatDate(String fecha, String formato) {
		boolean resp = true;
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		try {
			sdf.parse(fecha);
		} catch (Exception e) {
			resp = false;
			//logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo fecha: {} con formato {} ", fecha, formato);
//		logger.debug("Validar campo fecha: {} es: {}", fecha,  resp);
		return resp;
	}
	
	public static boolean validateEqualTo(String campo1, String campo2) {
		boolean resp = false;
		try {
			if(!StringUtils.isEmpty(campo1) && !StringUtils.isEmpty(campo2) 
					&& campo1.equals(campo2)){
				resp = true;
			}
		} catch (Exception e) {
			resp = false;
			//logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campos, campo1: {} con campo2 {} ", campo1, campo2);
//		logger.debug("Validar campos, campo1: {} es: {}", campo1,  resp);
		return resp;
	}
	
	/**
     * Validate given email with regular expression.
     *
     * @param email
     *            email for validation
     * @return true valid email, otherwise false
     */
	public static boolean validateEmail(String email) {
		boolean resp = false;
		try {
	        resp = patternMail.matcher(email).matches();
		} catch (Exception e) {
			logger.error("Error al validar campo: {}", e.getMessage());
		}
//		logger.debug("Validar campo email: {} es: {}", email, resp);
		return resp;
	}

	public static String getCadenaBase64(String cadena){
		String cad = "";
		String respuesta = null;
		try
		{
			logger.debug("cadena: " + cadena);
			
			byte[] cadenaBytes= cadena.getBytes();		
			
			for(int i = 0; i < cadenaBytes.length; i ++){
				cad += cadenaBytes[i];
			}
			logger.debug("cadena bytes:" + cad);
			
//			byte[] md5c = DigestUtils.md5(certificado);
			byte[] base64 = Base64.encodeBase64(cadenaBytes);
			logger.debug("cadena base64: " + base64);
			
			respuesta = new String(base64);
		}catch(Exception e){
			logger.error("Error en getCadenaBase64: ", e);
		}
		return respuesta;
	}
	
	public static String getBase64Cadena(String cadena){
		String cad = "";
		String respuesta = null;
		try{
			logger.debug("cadena: " + cadena);
			respuesta = new String (Base64.decodeBase64(cadena.getBytes()));
			
			logger.debug("final: " + respuesta);
		}catch(Exception e){
			logger.error("Error en getBase64Cadena: ", e);
		}
		return respuesta;
	};
	
	public static String getCertificadoMD5(SolicitudCompraVO compraVO, String secureCode){
		StringBuffer cadena = new StringBuffer();
		String respuesta = null;
		String cad = "";
		try
		{
			cadena.append(compraVO.getIdComercio()).append("^")
			.append(secureCode).append("^")
			.append(compraVO.getNumeroControl()).append("^")
			.append(formatoImporte(compraVO.getTotal()));
			
//			logger.info("cadena: " + cadena);
			
			byte[] cadenaBytes= cadena.toString().getBytes();		
			for(int i = 0; i < cadenaBytes.length; i ++){
				cad += cadenaBytes[i] + " ";
			}
//			logger.debug("Cadena bytes:" + cad);
			cad = "";
			byte[] md5c = DigestUtils.md5(cadenaBytes);
			for(int i = 0; i < md5c.length; i ++){
				cad += md5c[i] + " ";
			}
//			logger.debug("Cadena bytes MD5:" + cad);
			cad = "";
			byte[] base64 = Base64.encodeBase64(md5c);
			for(int i = 0; i < base64.length; i ++){
				cad += base64[i] + " ";
			}
//			logger.debug("Cadena bytes Base64:" + cad);
			
			respuesta = new String(base64);
//			logger.info("Certificado: " + respuesta);
		}catch(Exception e){
			respuesta = new String();
			logger.error("Error en getCadenaBase64: ", e);
		}
		return respuesta;
	}
	
	private static final String patronImp = "########0.00";
	private static DecimalFormat formato;
	private static DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
	
	static{
		simbolos.setDecimalSeparator('.');
		formato = new DecimalFormat(patronImp,simbolos);
	}
	
	public static String formatoImporte(BigDecimal numDecimal){
		if(numDecimal == null){
			return "0.00";
		}
		return formatoImporte(numDecimal.doubleValue());
	}
	
	public static String formatoImporte(String numString){
		String total = null;
		if(!StringUtils.isEmpty(numString)){
			total = formatoImporte(Double.parseDouble(numString));
		}
		return total;
	}
	
	public static String formatoImporte(double numDouble){
		String total = null;
		try{
			total = formato.format(numDouble);
		}catch(Exception e){
			logger.error("Error en formatoImporte: "+ e.getMessage());
		}
		return total;
	}
	
}
