package com.addcel.portal.pagos.utils;

import java.util.HashMap;

public class UtilsConstants {
	
	public static final String URL_AUT_PROSA = "https://localhost:8443/ProsaWeb/ProsaAuth?";
	public static final String URL_AUT_AMEX = "https://localhost:8443/AmexWeb/AmexAuthorization";
	public static final String URL_MAIL = "https://50.57.192.214:8443/MailSenderAddcel/enviaCorreoAddcel";
	
		
	
	public static final String VAR_PAYW_URL_BACK = "https://www.mobilecard.mx:8443/VPOS/portal/payworksRespuesta";//TODO cambiar en produccion.
	//public static final String VAR_PAYW_URL_BACK = "https://50.57.192.214:8443/VPOS/portal/payworksRespuesta";//QA

	/*Modo Producción AUT , 
	 * Modo de prueba autorizando siempre DEC, 
	 * Modo de prueba declinando siempre RND, 
	 * Modo de prueba con autorización aleatoria.
	 */
	public static final String VAR_PAYW_MODO = "PRD"; 	//TODO cambiar en produccion. PRD 
	public static final String VAR_PAYW_MERCHANT = "7267195"; //TODO cambiar para cada proyecto
	public static final String VAR_PAYW_USER = "a7267195";
	public static final String VAR_PAYW_PASS = "ldsn7195";
	public static final String VAR_PAYW_TERMINAL_ID = "72671951";
	

	
	public static final int STATUS_ACTIVO = 1;
	public static final int STATUS_CAMBIO_PASSWORD = 2;
	public static final int STATUS_BLOQUEADO = 3;
	public static final int STATUS_INACTIVO = 4;
	
	public static final int ID_ERROR_FINSESION = 1000;
	public static final String DESC_ERROR_FINSESION = "No se registro actividad, hemos cerramos la sesión para proteger la información."
			+ "<BR><BR>Regrese al sitio previo a esta pagina (sin utilizar el botón Atrás del navegador) y comience nuevamente su compra.";
	
//	public static final String KEY_ID_PROMARINE = "34c97e132611f46d50c8eb9ac806529c";
//	public static final String KEY_SECRET_PROMARINE = "a494a313d3f1a8d61f2236f8c7c69c5d";
	
//	public static final String URL_CONSULTA_PROMARINE = "https://becontrol.mx/api/";
//	public static final String URL_NOTIFICACION_PROMARINE = "https://becontrol.mx/api/";
	
//    public static final String VAR_MERCHANT = "7627488";
//	public static final String VAR_ORDER_ID = "";
//	public static final String VAR_STORE ="1234";
//	public static final String VAR_TERM = "001";
//	public static final String VAR_CURRENCY = "484";
//	public static final String VAR_ADDRESS = "PROSA";
//	public static final String VAR_URL_BACK = "http://50.57.192.214:8080/MobilecardSJGSBridge/ProsaRespuesta";
//	
	public static HashMap<String, String> errorPayW = new HashMap<String, String>();
	
	static{
		errorPayW.put("200","Indica que la transacción es segura y se puede enviar a Payworks.");
		errorPayW.put("201","Se detecto un error general en el sistema de Visa o Master Card, favor de esperar unos momentos para reintentar la transacción.");
		errorPayW.put("421","El servicio 3D Secure no está disponible, favor de esperar unos momentos para reintentar la transacción.");
		errorPayW.put("422","Se produjo un problema genérico al momento de realizar la Autenticación.");
		errorPayW.put("423","La Autenticación de la Tarjeta no fue exitosa.");
		errorPayW.put("424","Autenticación 3D Secure no fue completada, no está ingresando correctamente la contraseña 3D Secure.");
		errorPayW.put("425","Autenticación Inválida, no está ingresando correctamente la contraseña3D Secure.");
		errorPayW.put("430","Tarjeta de Crédito nulo, la numero de Tarjeta se envió vacía.");
		errorPayW.put("431","Fecha de expiración nulo, la fecha de expricacion se envió vacía.");
		errorPayW.put("432","Monto nulo, la variable Total se envió vacía.");
		errorPayW.put("433","Id del comercio nulo, la variable MerchantId se envió vacía.");
		errorPayW.put("434","Liga de retorno nula, la variable ForwardPath se envió vacía.");
		errorPayW.put("435","Nombre del comercio nulo, la variable MerchantName se envió vacía.");
		errorPayW.put("436","Formato de TC incorrecto, la variable Card debe ser de 16 dígitos.");
		errorPayW.put("437","Formato de Fecha de Expiración incorrecto, la variable Expires debe tener el siguiente formato: YY/MM donde YY se refiere al año y MM se refiere al mes de vencimiento de la tarjeta.");
		errorPayW.put("438","Fecha de Expiración incorrecto, indica que el plástico esta vencido.");
		errorPayW.put("439","Monto incorrecto, la variable Total debe ser un número menor a 999,999,999,999.## con la fracción decimal opcional, esta debe ser a lo más de 2 décimas.");
		errorPayW.put("440","Formato de nombre del comercio incorrecto, debe ser una cadena de máximo 25 caracteres alfanuméricos.");
		errorPayW.put("441","Marca de Tarjeta nulo, la variable CardType se envió vacía.");
		errorPayW.put("442","Marca de Tarjeta incorrecta, debe ser uno de los siguientes valores: VISA (para tarjetas Visa) o MC (para tarjetas Master Card).");
		errorPayW.put("443","CardType incorrecto, se ha especificado el CardType como VISA, sin embargo, el Bin de la tarjeta indica que esta no es Visa.");
		errorPayW.put("444","CardType incorrecto, se ha especificado el CardType como MC, sin embargo, el Bin de la tarjeta indica que esta no es Master Card.");
		errorPayW.put("446","Monto incorrecto, la variable Total debe ser superior a 1.0 pesos.");
		errorPayW.put("498","Transacción expirada. Indica que la transacción sobre paso el límite de tiempo de respuesta esperado.");
		errorPayW.put("499","Usuario excedió en tiempo de respuesta. Indica que el usuario tardo en capturar la información de 3D Secure mayor al tiempo esperado.");
	}

	
	
}
