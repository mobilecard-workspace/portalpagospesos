package com.addcel.portal.pagos.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.portal.pagos.model.vo.CorreoVO;
import com.addcel.portal.pagos.model.vo.PagoVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraVO;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String HTML_DOBY = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; font-weight: bold; }  a:hover { color: #FF0000; font-weight: bold; }  .page-container-add { width: 600px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; } .header-add { width: 600px; height: 82px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline;/*Fix IE floating margin bug*/; float: left; width: 540px; margin: 20px 30px 10px 30px; overflow: visible !important/*Firefox*/; overflow: hidden/*IE6*/; } .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); font-weight: normal; color: #000000; font-size: 11px; } .footer-add { clear: both; width: 600px; padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important/*Firefox*/; overflow: hidden/*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 104%; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br> <div class=\"main-content-add\"> <h1 class=\"pagetitle-add\">Estimado(a): @NOMBRE</h1> <p> Gracias por realizar su <strong>PAGO @EMPRESA</strong> con nosotros, <strong> MobileCard</strong>. </p> <p align=center>El Pago ha sido efectuado con &eacute;xito.</p> <p>&nbsp;</p> <p> <font size=+1>Confirmaci&oacute;n de Compra. </font> </p> <h3>&nbsp;</h3> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Fecha y Hora:</td> <td>@FECHA</td> </tr> <tr> <td>Monto:</td> <td>$ @MONTO</td> </tr> @COMISION <tr> <td>Cargo aplicado:</td> <td>$ @CARGO</td> </tr> <tr> <td>N&uacute;mero Referencia MC:</td> <td>@REFERENCIAMC</td> </tr> <tr> <td>N&uacute;mero Referencia @EMPRESA:</td> <td>@REFERENCIA</td> </tr> <tr> <td>Autorizaci&oacute;n bancaria:</td> <td>@AUTORIZACION</td> </tr> </tbody> </table> <p>&nbsp;</p> <p align=center> <font size=-1>¡Gracias por usar MobileCARD!</font> </p> <p> Para dudas y/o aclaraciones puedes contactarnos a trav&eacute;s de los tel&eacute;fonos +52 (55) 55403124 o escribenos a <a href=mailto:soporte@addcel.com>soporte@addcel.com</a> </p> <p> Tambi&eacute;n puedes visitarnos en nuestra p&aacute;gina web: <a href=\"https://www.mobilecard.mx\" title=\"Visistar p&aacute;gina MobilieCard\">www.mobilecard.mx</a> </p> <h3>&nbsp;</h3> </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>MobileCard &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html>";
	
	private static final String HTML_DOBY_REF = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; font-weight: bold; }  a:hover { color: #FF0000; font-weight: bold; }  .page-container-add { width: 600px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .header-add { width: 600px; height: 82px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline; /*Fix IE floating margin bug*/; float: left; width: 540px; margin: 20px 30px 10px 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); font-weight: normal; color: #000000; font-size: 11px; }  .footer-add { clear: both; width: 600px; padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 104%; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br> <div class=\"main-content-add\"> <h1 class=\"pagetitle-add\">Estimado(a): @NOMBRE</h1> <p> Para completar su pago de <strong>@EMPRESA</strong> en Tiendas de conveniencia tiene que utilizar la referencia de pago <strong>PAYNET</strong> &oacute; el c&oacute;digo de barras. </p> <p>&nbsp;</p> <p> <font size=+1>Confirmaci&oacute;n de generaci&oacute;n de Referencia. </font> </p> <h3>&nbsp;</h3> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Fecha y Hora:</td> <td>@FECHA</td> </tr> <tr> <td>Monto:</td> <td>$ @MONTO MXN</td> </tr> @COMISION <tr> <td>Cargo aplicado:</td> <td>$ @CARGO MXN</td> </tr> <tr> <td>Numero Referencia MC:</td> <td>@REFERENCIAMC</td> </tr> <tr> <td>Numero Referencia @EMPRESA:</td> <td>@REFERENCIA</td> </tr> <tr> <td>Referencia para Pago de: <img src=\"cid:identifierCID01\"></td> <td>@REF_DIESTEL</td> </tr> <tr colspan=\"2\"> <td><img src=\"http://201.147.99.60/PaynetCE/GetBarcodeImage.pn?text=@REF_DIESTEL&bh=80&bw=2&Itype=1\"></td> </tr> <tr> <td>Pasos a seguir para realizar el pago:</td> <td><b>1.</b>&nbsp;&nbsp;Acude a una de las tiendas afiliadas.<BR /> <b>2.</b>&nbsp;&nbsp;Menciona al cajero que realizarás un pago de <strong>servicio PAYNET.</strong><BR /> <b>3.</b>&nbsp;&nbsp;Entrega el código de barras o dicta tu referencia <strong>Paynet</strong> y realiza el pago en efectivo  (más $8.00 pesos por comisión). <BR /> <b>4.</b>&nbsp;&nbsp;Conserva el <strong>ticket</strong> para cualquier aclaración.</td> </tr> </tbody> </table> <p>&nbsp;</p> <p>Puede realizar su pago en las siguientes tiendas:</p> <img src=\"cid:identifierCID02\"> <!-- <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td></td> </tr> </tbody> </table> --> <p>&nbsp;</p> <p> Para dudas y/o aclaraciones puedes contactarnos a trav&eacute;s de los telefonos +52 (55) 55403124 o escribenos a <a href=mailto:soporte@addcel.com>soporte@addcel.com</a> </p> <p> Tambien puedes visitarnos en nuestra p&aacute;gina web: <a href=\"https://www.mobilecard.mx\" title=\"Visistar pagina MobilieCard\">www.mobilecard.mx</a> </p> <h3>&nbsp;</h3> </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>MobileCard &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html>";
			

	public static CorreoVO generatedMail(SolicitudCompraRespuestaVO res, SolicitudCompraVO solicitudCompra, PagoVO pago, String empresa){
//		logger.info("Genera objeto para envio de mail: " + pago.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
//			logger.info("datos getNombre: " + (pago != null? pago.getNombre() : "Usuario"));
//			logger.info("datos getFechaTransaccion: " + (res != null ?res.getFechaTransaccion(): "0"));
//			logger.info("datos getImporteForm: " + (solicitudCompra != null? solicitudCompra.getImporteForm(): "0.00"));
//			logger.info("datos getComisionForm: " + (solicitudCompra != null && solicitudCompra.getComisionForm() != null? solicitudCompra.getComisionForm(): "0.00"));
//			logger.info("datos getTotalPagoForm: " + (solicitudCompra != null? solicitudCompra.getTotalPagoForm(): "0.00"));
//			logger.info("datos getIdBitacora: " + (solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0"));
//			logger.info("datos getNumeroControl: " + (solicitudCompra != null? solicitudCompra.getNumeroControl(): "0"));
//			logger.info("datos getAutorizacionBancaria: " + (res.getAutorizacionBancaria()!= null ? res.getAutorizacionBancaria() : ""));
//			logger.info("datos getEmail: " + (pago!= null ? pago.getEmail() : solicitudCompra != null?solicitudCompra.getEmail(): ""));
//			logger.info("datos getIdBitacora: " + (solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0"));
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("@EMPRESA", empresa);
			body = body.replaceAll("@NOMBRE", pago != null && pago.getNombre() != null? pago.getNombre() : "Usuario");
			body = body.replaceAll("@FECHA", res != null ?res.getFechaTransaccion(): "0");
			body = body.replaceAll("@MONTO", solicitudCompra != null? solicitudCompra.getImporteForm(): "0.00");
			body = body.replaceAll("@COMISION", (solicitudCompra != null && solicitudCompra.getComisionForm() != null? 
						"<tr> <td>Comisi&oacute;n:</td> <td>\\$  " + solicitudCompra.getComisionForm() + "</td> </tr>": ""));
			body = body.replaceAll("@CARGO", solicitudCompra != null? solicitudCompra.getTotalPagoForm(): "0.00");
			body = body.replaceAll("@REFERENCIAMC", solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0");
			body = body.replaceAll("@REFERENCIA", solicitudCompra != null? solicitudCompra.getNumeroControl(): "0");
			body = body.replaceAll("@AUTORIZACION", res.getAutorizacionBancaria()!= null ? res.getAutorizacionBancaria() : "");
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago MobileCard - Referencia MC: " + solicitudCompra.getIdBitacora();
			String[] to = {pago!= null ? pago.getEmail() : solicitudCompra != null?solicitudCompra.getEmail(): ""};
//			String[] to = {"jesus.lopez@addcel.com"};
			String[] cid = {
					"/usr/java/resources/images/Addcel/MobileCard_header_600.PNG"
					};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
//			correo.setCc(new String[]{"jesus.lopez@addcel.com","carlos@addcel.com"});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
//			correo = null;
			logger.error("Ocurrio un error al enviar el email: {}" + e);
			e.printStackTrace();
		}
		return correo;
	}
	
	
	public static CorreoVO generatedMail(SolicitudCompraRespuestaVO res, SolicitudCompraVO solicitudCompra, PagoVO pago){
//		logger.info("Genera objeto para envio de mail: " + pago.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
//			logger.info("datos getNombre: " + (pago != null? pago.getNombre() : "Usuario"));
//			logger.info("datos getFechaTransaccion: " + (res != null ?res.getFechaTransaccion(): "0"));
//			logger.info("datos getImporteForm: " + (solicitudCompra != null? solicitudCompra.getImporteForm(): "0.00"));
//			logger.info("datos getComisionForm: " + (solicitudCompra != null && solicitudCompra.getComisionForm() != null? solicitudCompra.getComisionForm(): "0.00"));
//			logger.info("datos getTotalPagoForm: " + (solicitudCompra != null? solicitudCompra.getTotalPagoForm(): "0.00"));
//			logger.info("datos getIdBitacora: " + (solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0"));
//			logger.info("datos getNumeroControl: " + (solicitudCompra != null? solicitudCompra.getNumeroControl(): "0"));
//			logger.info("datos getAutorizacionBancaria: " + (res.getAutorizacionBancaria()!= null ? res.getAutorizacionBancaria() : ""));
//			logger.info("datos getEmail: " + (pago!= null ? pago.getEmail() : solicitudCompra != null?solicitudCompra.getEmail(): ""));
//			logger.info("datos getIdBitacora: " + (solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0"));
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("@EMPRESA", "TruConnect");
			body = body.replaceAll("@NOMBRE", pago != null && pago.getNombre() != null? pago.getNombre() : "Usuario");
			body = body.replaceAll("@FECHA", res != null ?res.getFechaTransaccion(): "0");
			body = body.replaceAll("@MONTO", solicitudCompra != null? solicitudCompra.getImporteForm(): "0.00");
			body = body.replaceAll("@COMISION", (solicitudCompra != null && solicitudCompra.getComisionForm() != null? 
						"<tr> <td>Comisi&oacute;n:</td> <td>\\$  " + solicitudCompra.getComisionForm() + "</td> </tr>": ""));
			body = body.replaceAll("@CARGO", solicitudCompra != null? solicitudCompra.getTotalPagoForm(): "0.00");
			body = body.replaceAll("@REFERENCIAMC", solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0");
			body = body.replaceAll("@REFERENCIA", solicitudCompra != null? solicitudCompra.getNumeroControl(): "0");
			body = body.replaceAll("@AUTORIZACION", res.getAutorizacionBancaria()!= null ? res.getAutorizacionBancaria() : "");
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago MobileCard - Referencia MC: " + solicitudCompra.getIdBitacora();
			String[] to = {pago!= null ? pago.getEmail() : solicitudCompra != null?solicitudCompra.getEmail(): ""};
//			String[] to = {"jesus.lopez@addcel.com"};
			String[] cid = {
					"/usr/java/resources/images/Addcel/MobileCard_header_600.PNG"
					};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
//			correo.setCc(new String[]{"jesus.lopez@addcel.com","carlos@addcel.com"});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
//			correo = null;
			logger.error("Ocurrio un error al enviar el email: {}" + e);
			e.printStackTrace();
		}
		return correo;
	}
	
	public static CorreoVO generatedMailRefencia(SolicitudCompraRespuestaVO res, SolicitudCompraVO solicitudCompra, PagoVO pago){
//		logger.info("Genera objeto para envio de mail: " + pago.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY_REF.toString();
			body = body.replaceAll("@EMPRESA", "TruConnect");
			body = body.replaceAll("@NOMBRE", pago != null && pago.getNombre() != null? pago.getNombre() : "Usuario");
			body = body.replaceAll("@FECHA", res != null ?res.getFechaTransaccion(): "0");
			body = body.replaceAll("@MONTO", solicitudCompra != null? solicitudCompra.getImporteForm(): "0.00");
			body = body.replaceAll("@COMISION", (solicitudCompra != null && solicitudCompra.getComisionForm() != null? 
					"<tr> <td>Comisi&oacute;n:</td> <td>\\$  " + solicitudCompra.getComisionForm() + " MXN</td> </tr>": ""));
			body = body.replaceAll("@CARGO", solicitudCompra != null? solicitudCompra.getTotalPagoForm(): "0.00");
			body = body.replaceAll("@REFERENCIAMC", solicitudCompra != null? solicitudCompra.getIdBitacora()+"": "0");
			body = body.replaceAll("@REFERENCIA", solicitudCompra != null? solicitudCompra.getNumeroControl(): "0");
			body = body.replaceAll("@REF_DIESTEL", res.getReferencia()!= null ? res.getReferencia() : "");
			
			String from = "no-reply@addcel.com";
			String subject = "Pagos Tiendas Conveniencia - Referencia MC: " + solicitudCompra.getIdBitacora();
			String[] to = {pago!= null ? pago.getEmail() : solicitudCompra != null?solicitudCompra.getEmail(): ""};
//			String[] to = {"jesus.lopez@addcel.com"};
			String[] cid = {
					"/usr/java/resources/images/Addcel/MobileCard_header_600.PNG",
					"/usr/java/resources/images/Paynet/paynet_logo_25.png",
					"/usr/java/resources/images/Paynet/logos_tiendas_300.png"
					};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
//			correo.setCc(new String[]{"jesus.lopez@addcel.com","carlos@addcel.com"});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
//			correo = null;
			logger.error("Ocurrio un error al enviar el email: {}" + e);
			e.printStackTrace();
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(UtilsConstants.URL_MAIL);
			logger.info("Conectando con " + UtilsConstants.URL_MAIL);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
