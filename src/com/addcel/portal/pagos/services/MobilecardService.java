package com.addcel.portal.pagos.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.portal.pagos.model.mapper.BitacorasMapper;
import com.addcel.portal.pagos.model.mapper.MobilecardWebMapper;
import com.addcel.portal.pagos.model.vo.AbstractVO;
import com.addcel.portal.pagos.model.vo.CambioTarjeta;
import com.addcel.portal.pagos.model.vo.CompraVO;
import com.addcel.portal.pagos.model.vo.LoginRequest;
import com.addcel.portal.pagos.model.vo.PagoVO;
import com.addcel.portal.pagos.model.vo.PaymentData;
import com.addcel.portal.pagos.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraVO;
import com.addcel.portal.pagos.model.vo.TProveedorVO;
import com.addcel.portal.pagos.model.vo.TokenVO;
import com.addcel.portal.pagos.model.vo.UsuarioRequest;
import com.addcel.portal.pagos.model.vo.UsuarioVO;
import com.addcel.portal.pagos.model.vo.addcelBridge.Respuesta;
import com.addcel.portal.pagos.utils.UtilsConstants;
import com.addcel.portal.pagos.utils.UtilsService;
import com.addcel.portal.pagos.utils.UtilsValidate;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;

@Service
public class MobilecardService {
	private static final Logger logger = LoggerFactory.getLogger(MobilecardService.class);
	private static final String patron = "ddhhmmssSSS";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	private static final String patronComp = "yyyyMMddhhmmss";
	private static final SimpleDateFormat formatoComp = new SimpleDateFormat(patronComp);

	@Autowired
	private MobilecardWebMapper mapper;
	
//	@Autowired
//	private BitacorasMapper bitMapper;
//	
//	@Autowired
//	private UtilsService utilService;	
	
//	public String getToken(String json) {	
//		TProveedorVO tproveedor = null;
//		TokenVO token = null;
//		String respuesta = null;
//		try {
//			tproveedor = mapper.consultaTProveedor();
//		}catch (Exception pe) {
//			respuesta = "{\"idError\":11,\"mensajeError\":\"No existe configuracion para ver esta pagina.\"}";
//			logger.error("Error al consultar la funcion jsontransaccion: {}", pe);
//		}	
//		
//		try{
//			if(tproveedor != null ){
//				if(tproveedor.getStatus() != 1){
//					respuesta = "{\"idError\":12,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//					logger.error("Comercio no habilitado para realizar transaccion: {}");
//				}else{
//					try{
////						logger.debug("Llave para descifrar json.: {}", tproveedor.getClave3Desc());
////						logger.debug("descifrar json.: {}", json);
//						
//						json = AddcelCrypto.decryptTripleDes(tproveedor.getClave3Desc(), json);
//						
//						if(json == null){
//							respuesta = "{\"idError\":16,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//							logger.error("Error al descifrar JSON. ");
//						}else{
//							token = (TokenVO) utilService.jsonToObject(json, TokenVO.class);
//							
//							if(!tproveedor.getUsuario().equals(token.getUsuario()) || !tproveedor.getPassword().equals(token.getPassword())){
//								respuesta = "{\"idError\":15,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//								logger.error("Usuario no valido: {}, base {}", token.getUsuario(), tproveedor.getUsuario());
//								logger.error("Password no valido: {}, base {}", token.getPassword(), tproveedor.getPassword());
//								
//							}else{
////								respuesta="{\"token\":\""+AddcelCrypto.encrypt(UtilsConstants.KEY_TOKEN, mapper.getFechaActual(), false) +"\",\"idError\":0,\"mensajeError\":\"\"}";
//								respuesta="{\"token\":\""+AddcelCrypto.encrypt(UtilsConstants.KEY_TOKEN, Utilerias.getFullFechaActual(), false) +
//										"\",\"idError\":0,\"mensajeError\":\"\"}";
//							}
//						}
//					}catch(Exception e){
//						respuesta = "{\"idError\":17,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//						logger.error("Error al convertir json a objeto CompraVO.: {}",e);
//					}
//				}
//				respuesta = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), respuesta, false);
//			}else{
//				respuesta = "{\"idError\":18,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//				logger.error("Comercio no habilitado para realizar transaccion: {}");
//			}
//		}catch (Exception pe) {
//			respuesta = "{\"idError\":19,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
//			logger.error("Error al insertar usuario: {}", pe);
//		}finally{
//			logger.debug("Toekn respuesta: " + respuesta);
//		}
//		return respuesta;	
//	}
	
	public ModelAndView pagoInicio(ModelMap modelo, HttpSession session, HttpServletRequest request) {	
		ModelAndView mav = null;
		PagoVO pago = null;
		String usr = null;
		
		try{
			//session.invalidate();
			modelo.clear();
			pago = new PagoVO();

			logger.debug("Iniciar pantalla, inicio-pago" );
			mav = new ModelAndView("portal/pago-inicio");
			usr = (request.getUserPrincipal()).getName();
			
			mav.addObject("tproveedor", mapper.consultaTProveedor(usr));
			
			mav.addObject("pagoForm", pago);
			
		}catch(Exception e){
			logger.debug("Iniciar pantalla, error" );
			logger.debug("error: " + e.getMessage() );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		} finally{
			modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor(usr));
			mav.addObject("usr", usr);
			mav.addObject("tproveedor", modelo.get("tproveedor"));
		}
		
		return mav;	
	}
	
	private String notificacionComercio(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO res, PaymentData dataPay, ModelMap modelo){
		String notifiacionJson = null;
		StringBuilder data = new StringBuilder();
		TProveedorVO tproveedor = null;
		try{
			
			data.append("{\"idError\":").append(res.getIdError())
			.append(",\"mensajeError\":\"").append(res.getMensajeError())
			.append("\",\"numeroControl\":\"").append(solicitudCompra.getNumeroControl())
			.append("\",\"fechaTransaccion\":\"").append(res.getFechaTransaccion())
			.append("\",\"autorizacion\":\"").append(res.getAutorizacionBancaria())
			.append("\",\"transaccionMC\":").append(solicitudCompra.getIdBitacora())
			.append(",\"referencia\":\"").append(res.getReferencia())
			.append("\",\"tarjeta\":\"").append(dataPay.getCardNumber() )
			.append("\",\"plazo\":").append(dataPay.getPlazo())
			.append(",\"pago\":").append(false)
			.append(",\"canal\":").append(solicitudCompra.getCanal())
			.append(",\"modoPago\":").append(solicitudCompra.getModoPago())
			.append("}");
			
			tproveedor = modelo.get("tproveedor") != null? (TProveedorVO)modelo.get("tproveedor"): mapper.consultaTProveedor((String)modelo.get("usr"));
			notifiacionJson = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), data.toString());
//			utilService.notificacionURLComercio(solicitudCompra.getUrlBack() + "?param=" + notifiacionJson, null);
			res.setJsonNotificacion(notifiacionJson);
			
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return notifiacionJson;
	}
	
	
	/**
	 * @param session
	 * @return
	 */
	public ModelAndView logout(ModelMap modelo, HttpSession session, HttpServletRequest request){
		
		if(session != null){
			session.removeAttribute("j_username");
			session.invalidate();
			modelo.clear();
		}
		
		ModelAndView mav = new ModelAndView("redirect:/");
		
		return mav;
	}
	
}
