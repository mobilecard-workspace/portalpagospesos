package com.addcel.portal.pagos.services;

import java.net.URLEncoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.portal.pagos.model.mapper.BitacorasMapper;
import com.addcel.portal.pagos.model.mapper.MobilecardWebMapper;
import com.addcel.portal.pagos.model.vo.PagoVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraVO;
import com.addcel.portal.pagos.model.vo.pagos.CatalogoBinVO;
import com.addcel.portal.pagos.model.vo.pagos.ProsaPagoVO;
import com.addcel.portal.pagos.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.portal.pagos.model.vo.pagos.TBitacoraVO;
import com.addcel.portal.pagos.utils.AddCelGenericMail;
import com.addcel.portal.pagos.utils.UtilsConstants;
import com.addcel.portal.pagos.utils.UtilsService;
import com.addcel.utils.Utilerias;

@Service
public class MobilecardOpenSwitchService {
	private static final Logger logger = LoggerFactory.getLogger(MobilecardOpenSwitchService.class);
	private MobilecardWebMapper mapper;
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private BitacorasMapper bitMapper;

	

	
	
	private void updateBitacoras(long idBitacora, ProsaPagoVO prosaPagoVO, String tipoTDC){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
			
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(idBitacora + "" );
        	tbitacoraVO.setBitConcepto(prosaPagoVO.getMsg());
        	tbitacoraVO.setBitTicket(prosaPagoVO.getMsg());
        	tbitacoraVO.setBitNoAutorizacion(prosaPagoVO.getAutorizacion());
        	tbitacoraVO.setBitStatus(prosaPagoVO.getStatus());
        	tbitacoraVO.setBitCodigoError(prosaPagoVO.getError() != 0? prosaPagoVO.getError(): 1);
//        	tbitacoraVO.setDestino("Id Transaccion: " + pago.getNumeroControl() + "-" + tipoTDC);
        	
        	bitMapper.updateBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(idBitacora + "");
        	tbitacoraProsaVO.setConcepto(prosaPagoVO.getMsg());
        	tbitacoraProsaVO.setAutorizacion(prosaPagoVO.getAutorizacion());
        	
        	bitMapper.updateBitacoraProsa(tbitacoraProsaVO);
        	
        	logger.info("Fin Update TBitacoraProsa.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
	}
	
	
	public String tipoTDC(String encryptedTDC){
		String tipoTDC = "NI";
		List<CatalogoBinVO> bines = null;		
		try{
			String TDC = encryptedTDC;
			
			bines = mapper.selectTipoTarjeta(
					TDC.substring(0, 6),
					TDC.substring(0, 7),
					TDC.substring(0, 8),
					TDC.substring(0, 9),
					TDC.substring(0, 10));
			
			if(bines!=null && bines.size()>0){
				tipoTDC = bines.get(0).getTipo();					
			}
		} catch(Exception e){
			logger.error("No se pudo obtener el tipo de TDC: {}",e.getMessage());
		}
		return tipoTDC;
	}
	
	public SolicitudCompraRespuestaVO switchAbiertoVISA(SolicitudCompraVO solicitudCompra, PagoVO pago, String afiliacion ){
		SolicitudCompraRespuestaVO res = new SolicitudCompraRespuestaVO();
		ProsaPagoVO prosaPagoVO = null;
		// Variable para guardar el status de confirmación de EdoMex: 1 Exitoso (pago y notificacion), 3 Pendiente (pago existoso y notificacion pendiente)
		int statusExito = 1;
		String tipoTDC = "NI";
		String sb = null;
		
		try{
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(pago.getTarjeta(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(pago.getMes() + "/" + pago.getAnio().substring(2,4), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(pago.getNombre(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(pago.getCvv2(), "UTF-8") )
				    .append( "&monto="    ).append( URLEncoder.encode(String.valueOf(solicitudCompra.getTotalPago()) + "", "UTF-8") )
					//.append( "&monto="    ).append( URLEncoder.encode("1.00" + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(afiliacion, "UTF-8") );
//					.append( "&moneda="   ).append( URLEncoder.encode(datosPago.getMoneda(), "UTF-8")) ;
					
			logger.info("Envío de datos VISA: {}", data);
			tipoTDC = tipoTDC(pago.getTarjeta());
			
			sb = utilService.notificacionURLComercio(UtilsConstants.URL_AUT_PROSA, data.toString());
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb, ProsaPagoVO.class);
			res.setFechaTransaccion(Utilerias.getFullFechaActual());
			
			if(prosaPagoVO != null){
				res.setTransaccionMC(solicitudCompra.getIdBitacora()+"");
				
				if(prosaPagoVO.isAuthorized()){
					prosaPagoVO.setStatus(1);
					prosaPagoVO.setMsg("EXITO PAGO " + (pago.getTipoTarjeta() == 1? "VISA": pago.getTipoTarjeta() == 2? "MASTER": "AMEX") + " SWA AUTORIZADA");
					
//					res.setMensajeError("El pago fue exitoso. El comprobante ha sido enviado al correo electrónico registrado.");
					res.setAutorizacionBancaria(prosaPagoVO.getAutorizacion());
					res.setImporte(solicitudCompra.getTotal());
					res.setComision(solicitudCompra.getComision());
					res.setTotalPago(solicitudCompra.getTotalPago());
					
					logger.info("MobileCard statusExito : {}", statusExito);
					updateBitacoras(solicitudCompra, prosaPagoVO, tipoTDC);
							
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(res, solicitudCompra, pago)));
				}else if(prosaPagoVO.isRejected()){
					prosaPagoVO.setStatus(0);
					prosaPagoVO.setError(prosaPagoVO.getError());
					prosaPagoVO.setMsg("PAGO " + (pago.getTipoTarjeta() == 1? "VISA": "MASTER") + " SWA RECHAZADA");
					
					res.setIdError(4);
					res.setMensajeError("El pago fue rechazado. " + (prosaPagoVO.getClaveRechazo() != null? ", Clave: " +prosaPagoVO.getClaveRechazo(): "") +
										(prosaPagoVO.getDescripcionRechazo() != null? ", Descripcion: " +prosaPagoVO.getDescripcionRechazo(): ""));
					updateBitacoras(solicitudCompra, prosaPagoVO, tipoTDC);
					
				}else if(prosaPagoVO.isProsaError()){
					prosaPagoVO.setStatus(0);
					prosaPagoVO.setError(prosaPagoVO.getError());
					prosaPagoVO.setMsg("PAGO " + (pago.getTipoTarjeta() == 1? "VISA": "MASTER") + " SWA ERROR");
					
					res.setIdError(5);
					res.setMensajeError(prosaPagoVO.getMsg() != null ? prosaPagoVO.getMsg(): "Ocurrio un error en el Banco.");
					updateBitacoras(solicitudCompra, prosaPagoVO, tipoTDC);
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error durante el pago al banco: {}", e);
			prosaPagoVO = new ProsaPagoVO();
			prosaPagoVO.setStatus(0);
			prosaPagoVO.setMsg("Ocurrio un error durante el pago al banco");
			updateBitacoras(solicitudCompra, prosaPagoVO, tipoTDC);
			
			res.setIdError(10);
			res.setMensajeError("Ocurrio un error durante la llamada al Banco.");
		}
		return res;
	}
	
	
	private void updateBitacoras(SolicitudCompraVO solicitudCompra, ProsaPagoVO prosaPagoVO, String tipoTDC){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
			bitMapper.updateJCGSDetalle(solicitudCompra.getIdBitacora(), solicitudCompra.getReferenciaCJGS(),
					prosaPagoVO.getStatus(), 
					tipoTDC, prosaPagoVO.getAutorizacion(), 
					Integer.parseInt(prosaPagoVO.getTransactionId() != null? prosaPagoVO.getTransactionId(): "0"), null);
			
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraVO.setBitConcepto(prosaPagoVO.getMsg());
        	tbitacoraVO.setBitTicket(prosaPagoVO.getMsg());
        	tbitacoraVO.setBitNoAutorizacion(prosaPagoVO.getAutorizacion());
        	tbitacoraVO.setBitStatus(prosaPagoVO.getStatus());
        	tbitacoraVO.setBitCodigoError(prosaPagoVO.getError() != 0? prosaPagoVO.getError(): 1);
        	tbitacoraVO.setDestino("Id Transaccion: " + solicitudCompra.getNumeroControl() + "-" + tipoTDC);
        	
        	bitMapper.updateBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraProsaVO.setConcepto(prosaPagoVO.getMsg());
        	tbitacoraProsaVO.setAutorizacion(prosaPagoVO.getAutorizacion());
        	
        	bitMapper.updateBitacoraProsa(tbitacoraProsaVO);
        	
        	logger.info("Fin Update TBitacoraProsa.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
	}

}
