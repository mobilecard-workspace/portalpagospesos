package com.addcel.portal.pagos.services;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.portal.pagos.model.mapper.BitacorasMapper;
import com.addcel.portal.pagos.model.mapper.MobilecardWebMapper;
import com.addcel.portal.pagos.model.vo.AbstractVO;
import com.addcel.portal.pagos.model.vo.CompraVO;
import com.addcel.portal.pagos.model.vo.PagoVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.portal.pagos.model.vo.SolicitudCompraVO;
import com.addcel.portal.pagos.model.vo.TProveedorVO;
import com.addcel.portal.pagos.model.vo.UsuarioVO;
import com.addcel.portal.pagos.model.vo.pagos.CatalogoBinVO;
import com.addcel.portal.pagos.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.portal.pagos.model.vo.pagos.TBitacoraVO;
import com.addcel.portal.pagos.utils.AddCelGenericMail;
import com.addcel.portal.pagos.utils.UtilsConstants;
import com.addcel.portal.pagos.utils.UtilsService;
import com.addcel.portal.pagos.utils.UtilsValidate;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;

@Service
public class MobilecardPagoService {
	private static final Logger logger = LoggerFactory.getLogger(MobilecardPagoService.class);

	@Autowired
	private BitacorasMapper bitMapper;

	@Autowired
	private MobilecardWebMapper mapper;
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private MobilecardOpenSwitchService soService;
	
	
	public ModelAndView procesaPagoDirecto(PagoVO pago, ModelMap modelo) {
		ModelAndView mav = null;
//		SolicitudCompraRespuestaVO res = new SolicitudCompraRespuestaVO();
		String errores = null;
		UsuarioVO usuario = null;
		TProveedorVO tProveedorVO = null;
		try {
			
			/*
			logger.debug("pago.getTarjeta(): " + pago.getTarjeta());
			logger.debug("pago.getTmpTarjeta(): " + pago.getTmpTarjeta());			
			logger.debug("pago.getDet_tmpTarjeta(): " + pago.getDet_tmpTarjeta());
			*/
			tProveedorVO = (TProveedorVO)modelo.get("tproveedor");
			
			pago.setIdUsuario(tProveedorVO.getIdProveedor());
			pago.setTmpTarjeta(pago.getTarjeta());			

			errores = validateFieldsPagoDirecto(pago);
			if( errores != null){
				mav = new ModelAndView("portal/pago-inicio");
				
				logger.debug("NUMERO_TARJETA: " + pago.getTarjeta());
				
				modelo.put("pagoForm", pago);
				modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor((String)modelo.get("usr")));
				mav.addObject("mensajePagoDir", errores);
				
			}else{
				mav = procesaPago(pago, modelo);
				
			}
			mav.addObject("pago", pago);
			modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor((String)modelo.get("usr")));
		}catch (Exception pe) {
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + pe.getMessage());
		}finally{
			if(mav != null){
				modelo.put("pagoForm", pago);
//				mav.addObject("tproveedor", modelo.get("tproveedor"));
				modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor((String)modelo.get("usr")));
			}
		}
		return mav;
	}

	public ModelAndView procesaPago(PagoVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
//		SolicitudCompraRespuestaVO res = null;
		String afiliacion = null;
		try{
			afiliacion = buscaAfiliacion();
			
			if(afiliacion == null){
				mav = new ModelAndView("portal/pago-inicio");
//				res = new SolicitudCompraRespuestaVO(40, "No existe una afiliacion configurada.");
				mav.addObject("mensajePagoDir", "No existe una afiliacion configurada.");
			}else{
				
				insertaBitacoras(pago, afiliacion);
				mav = procesaPagoSwithAbierto(pago, afiliacion, modelo);
			}
//			mav.addObject("soliCompraResp", res);
//			modelo.put("soliCompraResp", res);		
		}catch(Exception e){
//			res = new SolicitudCompraRespuestaVO(100, "Ocurrio un error General al realizar el pago : <br>" + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPagoSwithAbierto(PagoVO pago, String afiliaciones, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO res = null;
		List<String> listaNegra = null;
		try{
			
			listaNegra = mapper.buscarListaNegra(AddcelCrypto.encryptTarjeta(pago.getTarjeta()));
			
			if(listaNegra != null && listaNegra.size() > 0){
				res = new SolicitudCompraRespuestaVO(-10, "No es posible realizar el pago, el numero de Tarjeta se encuentra en Lista Negra.");
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", "No es posible realizar el pago, el numero de Tarjeta se encuentra en Lista Negra.");
			}else{
				if(pago.getTipoTarjeta() != 3){
					//res = soService.switchAbiertoVISA(pago, afiliaciones );
					//TODO agregar aquí metodo para procesar pago pw2
					mav = procesaPagoPayWorks2(pago, modelo);
					
				}else{
					res = new SolicitudCompraRespuestaVO(41, "El Tipo de Tarjeta no es valido.");
				}
			}
		}catch(Exception e){
			res = new SolicitudCompraRespuestaVO(100, "Ocurrio un error General al realizar el pago : <br>" + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + e.getMessage());
		}finally{
			if(res != null){
				modelo.put("soliCompraResp", res);
				mav.addObject("soliCompraResp", res);
			}
			
		}
		return mav;
	}
	
	public ModelAndView pagoResultado(ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO soliCompra = null;
		UsuarioVO usuario = null;
		try {
			usuario = (UsuarioVO) modelo.get("usuario");
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			soliCompraResp = (SolicitudCompraRespuestaVO) modelo.get("soliCompraResp");

			if(soliCompra == null){
				logger.error("Dentro de login, se termino la session");
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
			}else{
				if(soliCompra.getUrlBack() == null || "".equals(soliCompra.getUrlBack())){
					soliCompraResp.setUrlConfirmacion("https://www.mobilecard.mx");
				}else{
					soliCompraResp.setUrlConfirmacion(soliCompra.getUrlBack());
				}
				
				mav.addObject("usuario", usuario);
				mav.addObject("soliCompra", soliCompra);
				mav.addObject("soliCompraResp", soliCompraResp);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		} finally{
//			mav.addObject("nameCompany", "promarine");
		}
		return mav;	
	}
	
	public ModelAndView procesaPagoPayWorks2(PagoVO pago, ModelMap modelo) {
		ModelAndView mav = null;
//		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		
		try{							
			resp.put("ID_AFILIACION", UtilsConstants.VAR_PAYW_MERCHANT);
			resp.put("USUARIO", UtilsConstants.VAR_PAYW_USER);
			resp.put("CLAVE_USR", UtilsConstants.VAR_PAYW_PASS);
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", UtilsConstants.VAR_PAYW_TERMINAL_ID);
			resp.put("MONTO", formatoMontoPayworks(pago.getImporte()));
			resp.put("MODO", UtilsConstants.VAR_PAYW_MODO);
			resp.put("NUMERO_TARJETA", pago.getTarjeta());
			resp.put("FECHA_EXP", pago.getMes() + pago.getAnio().substring(2,4));
			resp.put("CODIGO_SEGURIDAD", pago.getCvv2());
			resp.put("MODO_ENTRADA", "MANUAL");
			resp.put("URL_RESPUESTA", UtilsConstants.VAR_PAYW_URL_BACK);
			resp.put("IDIOMA_RESPUESTA", "ES");
				
			
			logger.debug("ID_AFILIACION: " + UtilsConstants.VAR_PAYW_MERCHANT);
			//logger.debug("USUARIO: " + UtilsConstants.VAR_PAYW_USER);
			//logger.debug("CLAVE_USR: " + UtilsConstants.VAR_PAYW_PASS);
			logger.debug("CMD_TRANS: " + "VENTA");
			logger.debug("ID_TERMINAL: " + UtilsConstants.VAR_PAYW_TERMINAL_ID);
			logger.debug("MONTO: " + formatoMontoPayworks(pago.getImporte()));
			logger.debug("MODO: " + UtilsConstants.VAR_PAYW_MODO);
			logger.debug("NUMERO_TARJETA: " + pago.getTarjeta().substring(pago.getTarjeta().length()-3, pago.getTarjeta().length()));			
			logger.debug("FECHA_EXP: " + pago.getMes() + pago.getAnio().substring(2,4));
			//logger.debug("CODIGO_SEGURIDAD: " + pago.getCvv2());
			logger.debug("MODO_ENTRADA: " + "MANUAL");
			logger.debug("URL_RESPUESTA: " + UtilsConstants.VAR_PAYW_URL_BACK);
			logger.debug("IDIOMA_RESPUESTA: " + "ES");
			
			//respBD = bitMapper.guardaTBitacoraPIN(solicitudCompra.getIdBitacora(), AddcelCrypto.encryptHard( pago.getCvv2(), false));
			//if(respBD == 1){
				mav = new ModelAndView("portal/comercioPAYW2");
				mav.addObject("pw2", resp);
			
			//}
			
		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			mav = new ModelAndView("error");
			mav.addObject("mensajePagoMeses", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView payWorksRespuesta(
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW,
			String ID_AFILIACION, ModelMap modelo) {
		
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();		
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;		
		String tipoTDC = "NI";
		String msg = null;
		String empresa = modelo.get("tproveedor")!= null? ((TProveedorVO)modelo.get("tproveedor")).getNombre() : "";
//		HashMap<String, String> resp = new HashMap<String, String>();
		
		try{
			
			logger.debug("REFERENCIA: " + REFERENCIA);
			logger.debug("FECHA_RSP_CTE: " + FECHA_RSP_CTE);
			logger.debug("TEXTO: " + TEXTO);
			logger.debug("RESULTADO_PAYW: " + RESULTADO_PAYW);
			logger.debug("FECHA_REQ_CTE: " + FECHA_REQ_CTE);
			logger.debug("CODIGO_PAYW: " + CODIGO_PAYW);
			logger.debug("CODIGO_AUT: " + CODIGO_AUT);
			logger.debug("ID_AFILIACION: " + ID_AFILIACION);
			
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			if(soliCompra == null)
				soliCompra = new SolicitudCompraVO();
			
			pago = (PagoVO) modelo.get("pago");
			soliCompraResp = new SolicitudCompraRespuestaVO();
			
			tipoTDC = tipoTDC(pago.getTarjeta());

			//bitMapper.borrarTBitacoraPIN(NUMERO_CONTROL);
					
			logger.debug("pago.getIdBitacora(): " + pago.getIdBitacora());
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				
				soliCompra.setIdBitacora(pago.getIdBitacora());
				soliCompra.setTotal(Double.parseDouble(pago.getImporte()) );
				soliCompra.setTotalPago(Double.parseDouble(pago.getImporte()) );
				soliCompra.setNumeroControl(Long.toString(pago.getIdBitacora()));
								
				
				msg = "EXITO PAGO " + (pago.getTipoTarjeta() == 1? "VISA": pago.getTipoTarjeta() == 2? "MASTER": "VISA") + " PAYW AUTORIZADA";
				soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
				soliCompraResp.setAutorizacionBancaria(CODIGO_AUT);
				soliCompraResp.setImporte(soliCompra.getTotal());
				soliCompraResp.setComision(0L);
				soliCompraResp.setTotalPago(soliCompra.getTotalPago());
				soliCompraResp.setStatus(1);							
				
				soliCompraResp.setTarjeta(pago.getTarjeta().substring(0, 6) + " XXXXXX " + pago.getTarjeta().substring(pago.getTarjeta().length() - 4));										
				
				AddCelGenericMail.sendMail(
						utilService.objectToJson(AddCelGenericMail.generatedMail(soliCompraResp, soliCompra, pago, empresa)));
				
			}else{
				
				msg = "PAGO " + (pago.getTipoTarjeta() == 1? "VISA": "MASTER") + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				soliCompraResp.setStatus(0);
				soliCompraResp.setIdError(CODIGO_PAYW != null? Integer.parseInt(CODIGO_PAYW.substring(5)): 2);
				soliCompraResp.setMensajeError("El pago fue rechazado. " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW: "") + ", Descripcion: " + TEXTO  );
			}
			
			logger.debug("pago.getIdBitacora(): " + pago.getIdBitacora());
			
			soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
			soliCompraResp.setTransaccionMC(String.valueOf(soliCompra.getIdBitacora()));
			
			updateBitacoras(soliCompra, soliCompraResp, tipoTDC, msg);
//			soliCompraResp.setJsonNotificacion(AddcelCrypto.encryptHard(utilService.objectToJson(soliCompraResp)));
			//notificacionComercio(soliCompra, soliCompraResp, pago, modelo);
		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			soliCompraResp = new SolicitudCompraRespuestaVO(100, "Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}finally{
			modelo.put("soliCompraResp", soliCompraResp);
			mav.addObject("soliCompraResp", soliCompraResp);
		}
		return mav;	
	}	
	
	public String formatoMontoPayworks(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+ "." + centavos;
        }else{
            varTotal=monto.concat(".00");
        } 
		logger.info("Monto a cobrar : "+varTotal);
		return varTotal;		
	}
	
	private void insertaBitacoras( PagoVO pago, String afiliaciones ) throws Exception{
		TBitacoraVO tb = new TBitacoraVO();
//		String tarj = null;
		try {
//			tarj = pago.getTarjeta();
//			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new TBitacoraVO(
					pago.getIdUsuario() + "", "PAGO BotonPagos " + (pago.getTipoTarjeta() ==3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					pago.getDescripcion(), 
					pago.getTarjeta()!= null && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "", "WEB", 
					(pago.getSoftware() != null? pago.getSoftware(): "Pago "), (pago.getModelo() != null? pago.getModelo(): "Pago "), "", 
					String.valueOf(pago.getImporte()) );
			bitMapper.addBitacora(tb);
			
			pago.setIdBitacora(Long.parseLong(tb.getIdBitacora()));
			
			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					pago.getIdUsuario() + "", null, null, null, "PAGO BotonPagos " + (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					pago.getImporte(), null , "0", "0");
			
			bitMapper.addBitacoraProsa(tbProsa);
			
			logger.debug("pago.getIdBitacora(): " + pago.getIdBitacora());
		} catch (Exception e) {
			logger.error("Error TenenciaServices.consume3DSecure,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private void updateBitacoras(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO soliCompraResp, String tipoTDC, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
			/*
			bitMapper.updateJCGSDetalle(solicitudCompra.getIdBitacora(), soliCompraResp.getReferencia(), 
					solicitudCompra.getModoPago() == 3? -1:
					soliCompraResp.getIdError() == 0? 1: 0, 
					tipoTDC, soliCompraResp.getAutorizacionBancaria(), 0, soliCompraResp.getTarjeta());
			*/		
			
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(soliCompraResp.getAutorizacionBancaria());
        	tbitacoraVO.setBitStatus(solicitudCompra.getModoPago() != 3? soliCompraResp.getStatus(): -1);
        	tbitacoraVO.setBitCodigoError(soliCompraResp.getIdError() != 0? soliCompraResp.getIdError(): 1);
        	tbitacoraVO.setDestino("Id Transaccion: " + solicitudCompra.getNumeroControl() + "-" + tipoTDC);
        	
        	bitMapper.updateBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(soliCompraResp.getAutorizacionBancaria());
        	
        	bitMapper.updateBitacoraProsa(tbitacoraProsaVO);
        	
        	logger.info("Fin Update TBitacoraProsa.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
	}
	
	public String tipoTDC(String tdc){
		String tipoTDC = "NI";
		List<CatalogoBinVO> bines = null;		
		try{
			if(tdc != null && tdc.length() > 10){
				bines = mapper.selectTipoTarjeta(
						tdc.substring(0, 6),
						tdc.substring(0, 7),
						tdc.substring(0, 8),
						tdc.substring(0, 9),
						tdc.substring(0, 10));
				
				if(bines!=null && bines.size()>0){
					tipoTDC = bines.get(0).getTipo();					
				}
			}else{
				logger.error("Tarjeta vacia: " + tdc );
			}
		} catch(Exception e){
			logger.error("No se pudo obtener el tipo de TDC: {}",e.getMessage());
		}
		return tipoTDC;
	}

	private String notificacionComercio(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO res, PagoVO pago, ModelMap modelo){
		String notifiacionJson = null;
		StringBuilder data = new StringBuilder();
		TProveedorVO tproveedor = null;
		try{
			
			data.append("{\"idError\":").append(res.getIdError())
			.append(",\"mensajeError\":\"").append(res.getMensajeError())
			.append("\",\"numeroControl\":\"").append(solicitudCompra.getNumeroControl())
			.append("\",\"fechaTransaccion\":\"").append(res.getFechaTransaccion())
			.append("\",\"autorizacion\":").append(res.getAutorizacionBancaria()!= null? "\"" +res.getAutorizacionBancaria() + "\"":null)
			.append(",\"transaccionMC\":").append(solicitudCompra.getIdBitacora())
			.append(",\"referencia\":\"").append(res.getReferencia())
			.append("\",\"tarjeta\":\"").append(res.getTarjeta() != null? res.getTarjeta():
					pago != null && pago.getTarjeta() != null && pago.getTarjeta().length() >15 ? pago.getTarjeta().substring(0, 6) + " XXXXXX " + pago.getTarjeta().substring(12): 
						pago != null && pago.getTarjeta() != null && pago.getTarjeta().length() == 15 ? pago.getTarjeta().substring(0, 6) + " XXXXX " + pago.getTarjeta().substring(11): "" )
			.append("\",\"plazo\":").append(pago != null? pago.getMsi(): "1")
			.append(",\"pago\":").append(false)
			.append(",\"canal\":").append(solicitudCompra.getCanal())
			.append(",\"modoPago\":").append(solicitudCompra.getModoPago())
			.append("}");
			
//			tproveedor = mapper.consultaTProveedor();
			tproveedor = modelo.get("tproveedor") != null? (TProveedorVO) modelo.get("tproveedor"): mapper.consultaTProveedor((String)modelo.get("usr"));
			notifiacionJson = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), data.toString(), true);
//			utilService.notificacionURLComercio(solicitudCompra.getUrlBack() + "?param=" + notifiacionJson, null);
			res.setJsonNotificacion(notifiacionJson);
			
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return notifiacionJson;
	}

	private String validateFieldsPago(PagoVO pago){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(pago.getIdUsuario()+"","IdUsuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "12",UtilsValidate.MAXLENGTH + "15"});
//			respuesta += UtilsValidate.validateFields(pago.getPassword(),"Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
			respuesta += UtilsValidate.validateFields(pago.getCvv2(),"CVV2", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "4"});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	private String validateFieldsPagoDirecto(PagoVO pago){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(pago.getNombre(),"Nombre", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
			respuesta += UtilsValidate.validateFields(pago.getEmail(),"Correo", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
			respuesta += UtilsValidate.validateFields(pago.getImporte(),"Importe", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "1",UtilsValidate.MAXLENGTH + "15"});
			respuesta += UtilsValidate.validateFields(pago.getTarjeta(),"Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "15",UtilsValidate.MAXLENGTH + "16"});
//				respuesta += UtilsValidate.validateFields(pago.getVigencia(),"Vigencia", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "5",UtilsValidate.MAXLENGTH + "10"});
			respuesta += UtilsValidate.validateFields(pago.getMes(),"Vigencia Mes", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "2",UtilsValidate.MAXLENGTH + "2"});
			respuesta += UtilsValidate.validateFields(pago.getAnio(),"Vigencia Año", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "4",UtilsValidate.MAXLENGTH + "4"});
			respuesta += UtilsValidate.validateFields(pago.getCvv2(),"Codigo Seguridad", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "4"});
			respuesta += UtilsValidate.validateFields(pago.getTipoTarjeta()+"","Tipo Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,});

			
			if(pago.getTipoTarjeta() <= 0){
				respuesta = (respuesta != null? respuesta:"" ) + "Por favor, seleccione un Tipo de Tarjeta."; 
			}
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	public String terminar(CompraVO compraVO, SolicitudCompraRespuestaVO transaccion, String jsonNotificacion) {
		String json = null;
		AbstractVO res = new AbstractVO();
		HashMap<String, String> data = null;
		try {
//			if(compraVO.getUrlConfirmacion() != null && !"".equals(compraVO.getUrlConfirmacion())){
//				data = new HashMap<String, String>();
//				data.put("idAplicacion", String.valueOf(compraVO.getIdAplicacion()));
//				data.put("respuesta", "");
//				mapper.jsontransaccion(data);
//				
//				String[] datos = data.get("respuesta").split(";");
//				if(!datos[0].equals("0")){
//					logger.error("Error al obtener claves no se puede notificar al Comercio: {}", datos[1]);
//				} else {
//					utilService.notificacionURLComercio(compraVO.getUrlComercio(), "json=" + jsonNotificacion);
//				}
//			}
		}catch (Exception pe) {
			res.setIdError(91);
			res.setMensajeError("Error General en el proceso de pago.");
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			
		} finally{
			json = utilService.objectToJson(res);
		}
		return json;
	}


	public String buscaAfiliacion(){
		String afiliaciones = null;
		String[] tipoAfil = null;
		String[] afil = null;
		String respAf = null;
		try{
			afiliaciones = mapper.selectAfiliacion();
//			if(afiliaciones != null){
//				tipoAfil = afiliaciones.split(":");
//				for(String cad: tipoAfil){
//					afil = cad.split(",");
//					if(String.valueOf(canal).equals(afil[0])){
//						if(tipoTarjeta != 3){
//							respAf = afil[1];
//						}else if(tipoTarjeta == 3){
//							respAf = afil[2];
//						}
//					}
//				}
//			}
		}catch(Exception e){
			logger.error("Error al buscar afiliaciones: {}", e.getMessage());
		}
		return afiliaciones;
	}

	
}
