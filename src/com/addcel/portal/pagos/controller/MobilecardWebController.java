package com.addcel.portal.pagos.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.portal.pagos.model.vo.PagoVO;
import com.addcel.portal.pagos.services.MobilecardPagoService;
import com.addcel.portal.pagos.services.MobilecardService;
import com.addcel.portal.pagos.utils.UtilsConstants;

@Controller
@SessionAttributes({"usuario","soliCompra", "soliCompraResp", "tproveedor", "pago"})
public class MobilecardWebController {
	
	private static final Logger logger = LoggerFactory.getLogger(MobilecardWebController.class);
	
	
	@Autowired
	private MobilecardPagoService mcPagosService;
	
	@Autowired
	private MobilecardService addcelService;
	
	@RequestMapping(value = "/"/*, method=RequestMethod.GET*/)
	public ModelAndView home(ModelMap modelo, HttpSession session, HttpServletRequest request) {	
		logger.info("Dentro del servicio: /");
		logger.info("usr: "+ session.getAttribute("j_username"));
		logger.info("pass: "+ session.getAttribute("j_password"));
		
		return addcelService.pagoInicio(modelo, session, request);	
	}
	
//	@RequestMapping(value = "/portal/getToken"/*, method=RequestMethod.POST*/)
//	public @ResponseBody String getToken(@RequestParam("uparam") String jsonEnc, ModelMap modelo, HttpSession session) {	
//		logger.debug("Dentro del servicio: /portal/getToken");
//		return addcelService.getToken(jsonEnc);	
//	}

	
	@RequestMapping(value = "/portal/pago-inicio"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoInicio(ModelMap modelo, HttpSession session, HttpServletRequest request) {	
		logger.debug("Dentro del servicio: /portal/pago-inicio");
		return addcelService.pagoInicio( modelo, session, request);	
	}
	
	
	@RequestMapping(value = "/portal/procesa-pago-directo"/*, method=RequestMethod.POST*/)
	public ModelAndView procesaPagoDirecto(@ModelAttribute PagoVO pago, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/procesa-pago-directo");
		return mcPagosService.procesaPagoDirecto(pago, modelo);	
	}
		
	@RequestMapping(value = "/portal/pago-resultado"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoResultado(@RequestParam("uparam") String jsonEnc, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/pago-resultado");
		return mcPagosService.pagoResultado(modelo);	
	}
	
	@RequestMapping(value = "/portal/payworksRespuesta"/*, method = RequestMethod.POST*/)
	public ModelAndView payworksRespuesta(			
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/payworksRespuesta");
		return mcPagosService.payWorksRespuesta(REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW,ID_AFILIACION, modelo);	
	}
	
		
	@RequestMapping(value = "/portal/pago-error"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoError(ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/pago-error");
		ModelAndView mav = new ModelAndView("portal/pago-error");
		mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
		return mav;	
	}
	
	@RequestMapping(value = "/logout"/*, method=RequestMethod.GET*/)
	public ModelAndView logout(ModelMap modelo, HttpSession session, HttpServletRequest request) {	
		logger.info("Dentro del servicio: /logout");
		logger.info("usr: "+ session.getAttribute("user"));
		logger.info("pass: "+ session.getAttribute("j_password"));
		
		return addcelService.logout(modelo, session, request);	
	}
	
}