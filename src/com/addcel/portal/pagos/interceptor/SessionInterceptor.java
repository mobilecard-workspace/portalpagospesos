package com.addcel.portal.pagos.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class SessionInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(SessionInterceptor.class);
	private static final String url =  "/portal/pago-error";

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
//		if(!request.getRequestURI().endsWith("/")){
//			if (request.getSession().getAttribute("soliCompra") == null) {
//				logger.info("URL Request: " + request.getRequestURI());
//				logger.info("Sesion no valida.... ");
//				if(request.getRequestURI().contains("json")){
//					logger.info("Peticion no valida enviada por json...");
//					response.setContentType("application/json;");
//					response.setHeader("Cache-Control", "no-cache");
////					response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
//					response.getWriter().write("{\"idError\": -1001, \"mensajeError\": \"No se registro actividad, hemos cerramos la sesión para proteger la información.\"}");
//				}else {
//					logger.info("Redireccionado a: " + request.getContextPath() + url);
//					response.sendRedirect( request.getContextPath() + url );
//					request.setAttribute("mensajeError", "No se registro actividad, hemos cerramos la sesión para proteger la información.");
//				}
//				return false;
//			}
//		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		super.postHandle(request, response, handler, modelAndView);
	}
	
}
