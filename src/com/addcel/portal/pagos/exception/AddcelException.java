package com.addcel.portal.pagos.exception;

public class AddcelException extends Exception{
	private static final long serialVersionUID = 3189077303403108422L;
	private int id; 
	
	public AddcelException(int id, String msg) {
        super(msg);
        this.id = id;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
