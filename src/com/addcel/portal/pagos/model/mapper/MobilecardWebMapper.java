package com.addcel.portal.pagos.model.mapper;


import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.portal.pagos.model.vo.AbstractLabelValue;
import com.addcel.portal.pagos.model.vo.LoginRequest;
import com.addcel.portal.pagos.model.vo.TProveedorVO;
import com.addcel.portal.pagos.model.vo.UsuarioVO;
import com.addcel.portal.pagos.model.vo.pagos.CatalogoBinVO;

public interface MobilecardWebMapper {
	
	public String getFechaActual();

	public Integer difFechaMin(String fechaToken);
	
			
	public TProveedorVO consultaTProveedor(@Param(value = "usr") String usr);
	
	public AbstractLabelValue  getTerminos(@Param(value = "idComercio") String idComercio);

	public UsuarioVO consultaUsuario(LoginRequest login);
	
	public UsuarioVO actualizaUsuarioPassword(
			@Param(value = "idUsuario") long idUsuario,
			@Param(value = "idUsrStatus") String idUsrStatus,
			@Param(value = "usrPwd") String usrPwd
			);
	
	
	public AbstractLabelValue consultaTerminos();
	
	public String selectAfiliacion( );
	
	public List<CatalogoBinVO> selectTipoTarjeta(
			@Param(value = "subStr6") String subStr6,
			@Param(value = "subStr7") String subStr7,
			@Param(value = "subStr8") String subStr8,
			@Param(value = "subStr9") String subStr9,
			@Param(value = "subStr10") String subStr10
			);
	
	public void jsontransaccion(@Param(value = "map") HashMap<String, String> map);
	
	public List<AbstractLabelValue> getCatalogoGenero();
	
	public List<AbstractLabelValue> getCatalogoPais();
	
	public List<AbstractLabelValue> getCatalogoOperador();
	
	public List<AbstractLabelValue> consultaMSI(@Param(value = "bin") String bin);
	
	public List<String> buscarListaNegra(@Param(value = "tarjeta") String tarjeta);
}
