package com.addcel.portal.pagos.model.vo.pagos;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RespuestaAmexVO {
	
	private String transaction;
	private String code;
	private String dsc;
	private String error;
	private String errorDsc;
	
	private String msg;
	private int status;

	public String getTransaction() {
		return transaction;
	}

	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDsc() {
		return dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorDsc() {
		return errorDsc;
	}

	public void setErrorDsc(String errorDsc) {
		this.errorDsc = errorDsc;
	}
}
