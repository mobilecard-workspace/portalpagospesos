
package com.addcel.portal.pagos.model.vo;

import com.addcel.portal.pagos.model.vo.AbstractVO;
import com.addcel.utils.Utilerias;

public class SolicitudCompraRespuestaVO extends AbstractVO {
	
	private String transaccionMC;
	private String autorizacionBancaria;
    private String fechaTransaccion;
    private double importe;
	private double comision;
	private double totalPago;
	private String descripcionCompra;
	private String referencia;
	private int status;
	private String tarjeta;
	
	private String importeForm;
	private String comisionForm;
	private String totalPagoForm;
    
    private String urlConfirmacion;
    private String jsonNotificacion;
    
    public SolicitudCompraRespuestaVO(){
    }
    public SolicitudCompraRespuestaVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
	public String getTransaccionMC() {
		return transaccionMC;
	}
	public void setTransaccionMC(String transaccionMC) {
		this.transaccionMC = transaccionMC;
	}
	public String getAutorizacionBancaria() {
		return autorizacionBancaria;
	}
	public void setAutorizacionBancaria(String autorizacionBancaria) {
		this.autorizacionBancaria = autorizacionBancaria;
	}
	public String getFechaTransaccion() {
		return fechaTransaccion;
	}
	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}
	public double getImporte() {
		return importe;
	}
	
	public void setImporte(double importe) {
		this.importe = importe;
		this.importeForm = Utilerias.formatoImporteMon(importe);
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
		this.comisionForm = Utilerias.formatoImporteMon(comision);
	}

	public double getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(double totalPago) {
		this.totalPago = totalPago;
		this.totalPagoForm = Utilerias.formatoImporteMon(totalPago);
	}
	public String getDescripcionCompra() {
		return descripcionCompra;
	}
	public void setDescripcionCompra(String descripcionCompra) {
		this.descripcionCompra = descripcionCompra;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getImporteForm() {
		return importeForm;
	}
	public void setImporteForm(String importeForm) {
		this.importeForm = importeForm;
	}
	public String getComisionForm() {
		return comisionForm;
	}
	public void setComisionForm(String comisionForm) {
		this.comisionForm = comisionForm;
	}
	public String getTotalPagoForm() {
		return totalPagoForm;
	}
	public void setTotalPagoForm(String totalPagoForm) {
		this.totalPagoForm = totalPagoForm;
	}
	public String getUrlConfirmacion() {
		return urlConfirmacion;
	}
	public void setUrlConfirmacion(String urlConfirmacion) {
		this.urlConfirmacion = urlConfirmacion;
	}
	public String getJsonNotificacion() {
		return jsonNotificacion;
	}
	public void setJsonNotificacion(String jsonNotificacion) {
		this.jsonNotificacion = jsonNotificacion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
    
}
