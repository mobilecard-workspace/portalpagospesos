/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.portal.pagos.model.vo.addcelBridge;

/**
 *
 * @author -
 */
public class Respuesta {
    private  int resultado;
    private String mensaje;

    /**
     * @return the resultado
     */
    public int getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
