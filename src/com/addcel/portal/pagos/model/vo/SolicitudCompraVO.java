package com.addcel.portal.pagos.model.vo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.utils.Utilerias;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SolicitudCompraVO extends AbstractVO{
	
	private long idBitacora;
	private int idComercio;
	
	private String email;
	private String numeroControl;
	private int canal;
	private int modoPago;
	private String descripcion;
	private String referenciaCJGS;
	private String vigencia;
	private String digitoVer;
	private boolean conMSI;
	private String arrayMeses;
	private List<AbstractLabelValue> lstMeses;
	
	private String certificado;
	private String urlBack;
	private String token;
	
	private double total;
	private double comision;
	private double totalPago;
	
	private String importeForm;
	private String comisionForm;
	private String totalPagoForm;
	private String[] meses;
	
	public SolicitudCompraVO(){
	}
	
	public SolicitudCompraVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
	
	public int getIdComercio() {
		return idComercio;
	}
	public void setIdComercio(int idComercio) {
		this.idComercio = idComercio;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	public String getUrlBack() {
		return urlBack;
	}
	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
		this.comisionForm = Utilerias.formatoImporteMon(comision);
	}

	public double getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(double totalPago) {
		this.totalPago = totalPago;
		this.totalPagoForm = Utilerias.formatoImporteMon(totalPago);
	}

	public String getImporteForm() {
		return importeForm;
	}

	public void setImporteForm(String importeForm) {
		this.importeForm = importeForm;
	}

	public String getComisionForm() {
		return comisionForm;
	}

	public void setComisionForm(String comisionForm) {
		this.comisionForm = comisionForm;
	}

	public String getTotalPagoForm() {
		return totalPagoForm;
	}

	public void setTotalPagoForm(String totalPagoForm) {
		this.totalPagoForm = totalPagoForm;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
		this.importeForm = Utilerias.formatoImporteMon(total);
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNumeroControl() {
		return numeroControl;
	}

	public void setNumeroControl(String numeroControl) {
		this.numeroControl = numeroControl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getModoPago() {
		return modoPago;
	}

	public void setModoPago(int modoPago) {
		this.modoPago = modoPago;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getDigitoVer() {
		return digitoVer;
	}

	public void setDigitoVer(String digitoVer) {
		this.digitoVer = digitoVer;
	}

	public boolean isConMSI() {
		return conMSI;
	}

	public void setConMSI(boolean conMSI) {
		this.conMSI = conMSI;
	}

	public int getCanal() {
		return canal;
	}

	public void setCanal(int canal) {
		this.canal = canal;
	}

	public String getReferenciaCJGS() {
		return referenciaCJGS;
	}

	public void setReferenciaCJGS(String referenciaCJGS) {
		this.referenciaCJGS = referenciaCJGS;
	}

	public String getArrayMeses() {
//		if(arrayMeses == null || "".equals(arrayMeses) || "null".equals(arrayMeses)){
//			arrayMeses = "3,6,9,12"; 
//		}
		return arrayMeses;
	}

	public void setArrayMeses(String arrayMeses) {
		this.arrayMeses = arrayMeses;
		
		if(arrayMeses != null && !"".equals(arrayMeses) &&  !"null".equals(arrayMeses)){
			this.meses = arrayMeses.split(",");
//		}else{
//			this.meses = new String[]{"3","6","9","12"};
		}
	}

	public String[] getMeses() {
		return meses;
	}

	public void setMeses(String[] meses) {
		this.meses = meses;
	}

	public List<AbstractLabelValue> getLstMeses() {
		return lstMeses;
	}

	public void setLstMeses(List<AbstractLabelValue> lstMeses) {
		this.lstMeses = lstMeses;
	}
	
	
}
