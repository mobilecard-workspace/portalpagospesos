/**
 * T
ipoSolicitudCompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.portal.pagos.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UsuarioRequest {
	
	private long usuario;
    private String login;
    private String password;
//    private String nacimiento;
    private String telefono;
    private String registro;
    private String nombre;
    private String apellido;
    private String materno;
//    private String direccion;
    private String tarjeta;
    private String vigencia;
//    private int banco;
    private int tipotarjeta;
    private int proveedor;
//    private int status;
    private String mail;
//    private String passwordS;
    private String imei;
//    private int idtiporecargatag;
//    private String etiqueta;
//    private String numero;
//    private int dv;
    private String tipo;
    private String software;
    private String modelo;
//    private String key;
    
//    private String sexo;
//    private String tel_casa;
//    private String tel_oficina;
//    private int id_estado;
//    private String ciudad;
//    private String calle;
//    private int num_ext;
//    private String num_interior;
//    private String colonia;
    private String cp;
    private String dom_amex;
    private String terminos;
    private int tipo_cliente;
	public long getUsuario() {
		return usuario;
	}
	public void setUsuario(long usuario) {
		this.usuario = usuario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public int getTipotarjeta() {
		return tipotarjeta;
	}
	public void setTipotarjeta(int tipotarjeta) {
		this.tipotarjeta = tipotarjeta;
	}
	public int getProveedor() {
		return proveedor;
	}
	public void setProveedor(int proveedor) {
		this.proveedor = proveedor;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getDom_amex() {
		return dom_amex;
	}
	public void setDom_amex(String dom_amex) {
		this.dom_amex = dom_amex;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	public int getTipo_cliente() {
		return tipo_cliente;
	}
	public void setTipo_cliente(int tipo_cliente) {
		this.tipo_cliente = tipo_cliente;
	}

    }
